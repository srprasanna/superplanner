/**
 * Created with PyCharm.
 * User: prasanna
 * Date: 21/8/13
 * Time: 5:22 PM
 * To change this template use File | Settings | File Templates.
 */


$(document).ready(function(){
    var i = 0;
    var height = new Array();
    $("div.package").each(function(index,elem){
        height.push($(this).height());
    });

    var max_of_array = Math.max.apply(Math, height);
    $("div.package").css("height",max_of_array+"px");

    $("input.option-radio").bind("change",function(){
        generateOutput();
    });

    // $("span.radio-span123").on("click",function(){
    //   $(this).prev().trigger("click");
    // alert($(this).prev().prop("checked"));
    //});
});

function generateOutput(){
    var optionValues = new Array();
    $("input.option-radio:checked").each(function(){
        optionValues.push($(this).val());
    })

    var jsonOptions = JSON.stringify(optionValues);
    $("input#package-description").val(jsonOptions);
}

function selectRadioButton(span){
    $(span).prev().trigger("click");
}