/**
 * @author prasanna
 */

$(document).ready(function(){
   $("#id_f_and_b").bind("change",function(){
        if($(this).is(":checked")){
            addPackageToQuote(quote_id);
        }else{
            removePackageFromQuote(quote_id);
        }
   }) ;
    handleForm();
});


function addPackageToQuote(quote_id){
    location.href="/quote/addfoodbev?quote_id="+quote_id;
}

function removePackageFromQuote(quote_id){
    var options = {
        url:"/quote/removefoodbev?quote_id="+quote_id,
        success:function(){

        }
    }
    sendGetRequest(options);
}

function handleForm(){
    $("#id_name").attr("required",true);
    $("#id_address").attr("required",true);
    $("#id_email").attr("type","email");
    $("#id_email").attr("required",true);
    $("#id_mobile").attr("type","tel")
    $("#id_mobile").attr("pattern","\\d{10}")
    $("#id_mobile").attr("required",true);
}