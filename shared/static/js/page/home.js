$(document).ready(function() {

    $("div#search-panel").css("background", "url(" + url.background + ") no-repeat");
    $("div#search-panel").css("background-size", "100%");

    $("input#id_date").val("dd/mm/yy");

    addDatePicker("input#id_date",{});

    $('input#id_time').timepicker({
        template : false,
        showInputs : false,
        minuteStep : 5
    });

    $("select#id_city").attr("data-style", "btn-transparent");
    $("select#id_locality").attr("data-style", "btn-transparent");
    $("select#id_occasion").attr("data-style", "btn-transparent");
    $("select#id_occasion").attr("data-help-title", "Click Here to see selected Occassions");
    $("select#id_guests").attr("data-style", "btn-white");
    $("select#id_guests").attr("data-help-title", "Guests");
    $("select#id_guests").attr("data-show-tick", "true");
    $("select#id_seating").attr("data-style", "btn-white");
    $("select#id_seating").attr("data-show-tick", "true");
    $("select#id_seating").attr("data-help-title", "Seating (optional)");
    $("select#id_environment").attr("data-style", "btn-white");
    $("select#id_environment").attr("data-help-title", "Venue Type (optional)");

    $("select").addClass("selectpicker");
    $("select.selectpicker").selectpicker();
    
    $("#id_locality").selectpicker('selectAll');
	$("select#id_occasion").selectpicker('selectAll');
	$("#id_environment").selectpicker('selectAll');;
	
    $(".btn-transparent").addClass("cmb-transparent");
	setWidth('[data-id="id_occasion"]');
    flipTickers();
    loadlocations();
    setactivevenuetype();

    loadCityInfo($("select#id_city option:selected").val());
    var temp = 0;
    myInterval = setInterval(function(){
        if(temp == 4){
            temp = changeBG(0);
        }else{
            temp = changeBG(++temp);
        }
    },112000);
    $("select#id_city").change(function() {
        loadlocations();
        setWidth('[data-id="id_city"]');
        var selected = $(this).find("option:selected").val();
        loadCityInfo(selected);
    });
    $("select#id_locality").change(function() {
        setWidth('[data-id="id_locality"]');
    });

    citiesArray[0] = "Change city";
    $("#id_city option").each(function(){
        citiesArray[$(this).val()]=$(this).text();
    });

    loadChangeCity();

    
    $(window).resize(function(){
        setactivevenuetype();
    });
    var arr = ['[data-id="id_city"]','[data-id="id_locality"]','[data-id="id_occasion"]'];

    for(var i in arr){
        setWidth(arr[i]);
    }
    setCity(known_location);
    selectAllOptionsForOccasion();
});

function setactivevenuetype() {
    var para = $(".venuetypesselected").parent();
    var height = parseInt($(".venuetypesselected").height()) + parseInt($(".venuetypesselected").css('padding-top')) + parseInt($(".venuetypesselected").css('padding-bottom')) + parseInt($(".venuetypesselected").css('margin-top')) + parseInt($(".venuetypesselected").css('margin-bottom')) + 12;

    para.height(height);
    para.css("background", "url(" + url.downArrow + ") no-repeat center bottom transparent");
    $("div#venuetypesmenu").height(height);
    var parent = $("#venuetypesmenu");
    var totalWidth = $("#venuetypesmenu").width();
    var subWidth = 0;
    $("#venuetypesmenu>div>div").each(function(){
        subWidth=subWidth+$(this).width();
    });
    if(totalWidth>subWidth){
        $("#venuetypesmenu>div").css("margin-left",((totalWidth-subWidth)/2)+"px");
    }
}

function changeBG(key){
    if(myInterval){
        clearInterval(myInterval);
    }
    $("div#search-panel").css('background','url('+imageUrls[key]+') no-repeat scroll 0px 0px / 100% auto transparent');
    $("div.selectBG").removeClass("selectedBG");
    $("div#selectBG"+key).addClass("selectedBG");
    $('#tickerwrapper').flipper('update',tickerText[key]);
    //$("#tickerwrapper").text(tickerText[key]);
    return key;
}



function changeBGCall(key){
    if(myInterval){
        clearInterval(myInterval);
    }
    changeBG(key);
}

function loadChangeCity(){
    var parent = $("#change-city");
    parent.empty();
    var select = $("<select id=\"extra-city\" data-style=\"btn-transparent\"></select>");
    for(var key in citiesArray){
        var option =  $("<option value=\""+key+"\">"+citiesArray[key]+"</option>");
        select.append(option);
    }
    parent.append(select);
    select.bind("change",function(){    	
        var selected = $(this).find("option:selected").val();
        var text =  $(this).find("option:selected").text();
        if(selected != 0 ){
            $("h2#popularvenues").html("Popular Venues in "+text);
            loadCityInfo(selected);
            loadChangeCity();
        }
    });
    select.selectpicker({
    	helpTitle: "Change city"
    });
}

function loadCityInfo(selectedCity){
    occassion = getSelectedOccassion();
    var options = {
        data:{city_id:selectedCity,occasion:occassion},
        type:"GET",
        dataType:"json",
        url:"/venues/popular",
        success:function(data){
            var i =0;
            var j=0;
            $("div#pop-list").html("<div class=\"span4\">		</div>		<div class=\"span8\">			<div class=\"row-fluid\">				<div class=\"span3\">				</div>				<div class=\"span3\">				</div>				<div class=\"span3\">				</div>				<div class=\"span3\">				</div>			</div>			<div class=\"row-fluid\" style=\"margin-top:1.864102564102564%;\">				<div class=\"span3\">				</div>				<div class=\"span3\">				</div>				<div class=\"span3\">				</div>				<div class=\"span3\">				</div>			</div>		</div>");
            var span4 = $("div#pop-list div.span4");
            var span8 = $("div#pop-list div.span8");
            for (var key in data){
                if(i == 0){
                    span4.append("<div class=\"img-wrapper uppercase\" onclick=\"location.href='/venue?venue="+key+"'\"><a class href=\"/venue?venue="+key+"\"><img src=\""+data[key].imgUrl+"\" style=\"width:100%;\"/></a><div class=\"image-caption\"><div class=\"row-fluid\"><div class=\"span12 text-center\"><div style=\"font-weight:bold;\">"+data[key].name+"</div><div>"+data[key].address1+"</div></div></div></div><div class=\"image-caption-flow text-center\"><div style=\"font-weight:bold;margin-top:40%;\">"+data[key].name+"</div><div style=\"font-weight:bold;margin-top:2%;\">"+data[key].address1+"</div><div style=\"margin-top:6%;\">"+data[key].address2+"</div></div></div>");
                }else{
                    $(span8).find("div.row-fluid>div.span3").each(function(index,elem){
                        if(index == i-1){
                            $(this).append("<div class=\"img-wrapper uppercase\" onclick=\"location.href='/venue?venue="+key+"'\"><a class href=\"/venue?venue="+key+"\"><img src=\""+data[key].imgUrl+"\" style=\"width:100%;\"/></a><div class=\"image-caption\"><div class=\"row-fluid\"><div class=\"span12 text-center\"><div style=\"font-weight:bold;\">"+data[key].name+"</div><div>"+data[key].address1+"</div></div></div></div><div class=\"image-caption-flow text-center\"><div style=\"font-weight:bold;margin-top:40%;\">"+data[key].name+"</div><div style=\"font-weight:bold;margin-top:2%;\">"+data[key].address1+"</div><div style=\"margin-top:6%;\">"+data[key].address2+"</div></div></div>");
                        }

                    });
                }
                i++;
            }
            loadSeeMore(selectedCity);
            loadEvents(selectedCity);
        }

    };
    sendGetRequest(options);
}

function getSelectedOccassion(){
    return $("div#venuetypesmenu input.venuetypesselected").parent().attr("rel");
}

function loadSeeMore(selectedCity){
    occasion = getSelectedOccassion();
    $("#see-more").attr("href","/search/?occasion="+occasion+"&city="+selectedCity);
}

function loadEvents(selectedCity){
    $("div#venuetypesmenu input").unbind("click");
    $("div#venuetypesmenu input").bind("click",function(){
        $("div#venuetypesmenu input").each(function(){
            $(this).parent().attr("style","");
            $(this).parent().css("float","left");
            $(this).removeClass("venuetypesselected");
        });
        $(this).addClass("venuetypesselected");
        setactivevenuetype();
        loadCityInfo(selectedCity);
    });

}

function flipTickers(){
    $('#tickerwrapper').flipper({
        type:'fall',
        speed:'normal',
        queueSuper:false
    });}


function viewAllVenues(){
    var city = $("#id_city option:selected").val();
    location.href="/getallvenues/?city="+city;
}

function setCity(known_city){
	if(known_city !=undefined && known_city != ""){
		var city_val = undefined;
		$("select#id_city option").each(function(){
			if($(this).html().toLowerCase() == known_city.toLowerCase()){
				city_val = $(this).val();
			}
		});
		$("select#id_city").val(city_val);
		$("select#id_city").selectpicker("refresh");
	}
}

function selectAllOptionsForOccasion(){
    var selector = $("select#id_occasion").parent().find("ul.dropdown-menu")
    selector.find("li:first").on("click",function(event){
        event.stopImmediatePropagation();
        if(!$(this).hasClass("selected")){
            selectAll();
        }else{
            deselectAll();

        }
    });

    var flag = 1;
    $("select#id_occasion").on("change", function () {
        flag = allOptionsSelected();
        if (flag == 0) {
            selector.find("li:first").removeClass("selected");
            $('select#id_occasion').find("option").eq(0).prop('selected', false);

        } else {
            selector.find("li:first").addClass("selected");
            $('select#id_occasion').find("option").eq(0).prop('selected', true);
        }
    })

}

function allOptionsSelected() {
    var count = 0;
    var total = 0;
    $("select#id_occasion option").each(function () {
        if ($(this).html() == 'All' || $(this).html() == "ALL") {
            return true;
        }
        total++;

        if (!$(this).prop("selected")) {
            count++;
        }

    });

    return (count > 0) ? 0 : 1;
}

function deselectAll(){
    $("select#id_occasion").selectpicker("deselectAll");
}

function selectAll(){
    $("select#id_occasion").selectpicker("selectAll");
}
