$(document).ready(function() {
    addDatePicker("input#id_date",{});

    getFavourite();
    $('input#id_time').timepicker({
        template : false,
        showInputs : false,
        minuteStep : 5
    });

    $("select").addClass("selectpicker");
    $("select#id_city").attr("data-style", "btn-white");
    $("select#id_locality").attr("data-style", "btn-white");
    $("select#id_guests").attr("data-style", "btn-white");
    $("select#id_seating").attr("data-style", "btn-white");
    $("select#id_environment").attr("data-style", "btn-white");

    $("select.selectpicker").selectpicker();
	
		
    $(".btn-transparent").addClass("cmb-transparent");

    loadlocations();

    $("select#id_city").change(function() {
        loadlocations();
        setWidth('[data-id="id_city"]');
    });
    $("select#id_locality").change(function() {
        setWidth('[data-id="id_locality"]');
    });


//	if (username != "") {
//		$("a#login-button").text("");
//		$("a#logout-button").text("HELLO\n" + username.toUpperCase());
//	} else {
//		$("a#login-button").text("LOG IN / SIGN UP");
//		$("a#logout-button").text("");
//	}
    loadData();
    var height = new Array();
    $("#static_form>div").each(function(){
        height.push($(this).height());
    })

    var max_of_array = Math.max.apply(Math, height);
    $("#static_form>div").css("height",max_of_array+"px");
    resetWidth();
});

function toggleSearchForm() {
    $("div#static_form").toggle();
    $("div#completed_form").toggle();
}

function loadData() {
    var selectorsArray = ["id_time","id_guests", "id_seating", "id_environment"];
    var data = new Array();
    for (var i = 0; i < selectorsArray.length; i++) {
        var parent = $("select#" + selectorsArray[i]).parent();
        var text = parent.find('.bootstrap-select .filter-option').text();
        data.push(text);
    }
    $("div#static_form div>input").each(function(index, elem) {
        if (index > 0) {
            var ind = index - 1;
            var text = data[ind];
            if (ind == 1) {
                text += " guests";
            }
            $(this).val(text);
        }
    });
    resetWidth();
}

function addShortlist(id, name, address1, imgPath, elem) {
    var parent = $("div#shortlist-holder");
    if (parent.find("div.span2.unselected").length > 0) {
        parent.find("div.span2.unselected").each(function() {
            $(this).find("input.venue_id").val(id);
            $(this).find("img.hotel_image").attr("src", "/media/" + imgPath);
            $(this).find("div.hotel_name_plain").html(name);
            $(this).find("div.hotel_address1").html(address1);
            $(this).removeClass('unselected');
            $(this).addClass('selected');
            $(elem).attr("disabled", true);
            return false;
        });
    } else {
        addModal(1, {
            title : "Unable to do this action !",
            body : "You can only select 3 venues for shortlist.",
            show : "true"
        });
    }
}

function shortlist(){
    var venue_ids = new Array();
    $("div#shortlist-holder input.venue_id").each(function(){
        var value = $(this).val();
        if(value != undefined && value != ""){
            venue_ids.push(parseInt(value));
        }
    });
    if(venue_ids.length>0){
        location.href="/shortlist?venue_ids="+JSON.stringify(venue_ids);
    }
}


function removeFromShortlist(elem){
    var element = $(elem);
    var parent = element.parent().parent();
    parent.removeClass("selected");
    parent.addClass("unselected");
    var venue_id = parent.find("input.venue_id").val();
    $("#"+venue_id+"-shortlist").attr("disabled",false);
    parent.find("img.hotel_image").attr("src","/static/img/superplanner/new_hotel.png");
    parent.find("div.hotel_name_plain").html("add another");
    parent.find("div.hotel_address1").html("<strong>VENUE</strong>");
    parent.find("input.venue_id").val("");
}

function resetWidth(){
    var arr = ['input#date','input#time','input#guests','input#seating','input#environment','[data-id="id_city"]','[data-id="id_locality"]']
    for(var i in arr){
        setWidth(arr[i]);
    }

}
