$(document).ready(function() {
    $('.star-rating').raty({
        readOnly: true,
        score: function() {
            return $(this).attr('data-score');
        }
    });
    $(".star").raty();
    $("#write-review").bind("click",function(){
       $(".review-container").toggle();
    });

    getFavourite();
});
