/**
 * @author prasanna
 */

function sendGetRequest(options) {
    var opts = {
        error : function(xhr, statusText, errorThrown) {
            try {
                console.log("Ajax Request is an error.");
            } catch(e) {
                alert("Ajax Request is an error.");
            }
        },
        type : "GET"
    };

    $.extend(opts, options);
    $.ajax(options);
}

function sendPostRequest(options) {
    var opts = {
        error : function(xhr, statusText, errorThrown) {
            try {
                console.log("Ajax Request is an error.");
            } catch(e) {
                alert("Ajax Request is an error.");
            }
        },
        type : "POST"
    };
    $.extend(opts, options);
    $.ajax(options);
}

function loadlocations() {
    if ($("select#id_city").val() == "") {
        $("div#id_locality").attr('disabled', true);
        $("h2#popularvenues").text("Popular venues");
    } else {
        var id = $("select#id_city").val();
        $("h2#popularvenues").text("Popular venues in " + $("select#id_city option:selected").html());
        var opts = {
            url : "/city/" + id + "/locations/",
            success : function(data) {
                $('select#id_locality').find('option').remove();

                for (var i = 0; i < data.length; i++) {
                    $('select#id_locality').append($("<option></option>").attr("value", data[i].id).text(data[i].name));
                }
				
                $("select#id_locality").selectpicker({
                	helpTitle: "Locations Selected"
                })
                $("select#id_locality").selectpicker('refresh');
                $("select#id_locality").selectpicker('selectAll');
                if(locality != undefined && locality != "" && locality != "None"){
                	$("select#id_locality").selectpicker('val',locality);
                }
                
                
                // var hasOption = $('select#id_locality option[value="' + locality + '"]');
                // if (hasOption.length == 0) {
                // } else {
                    // setBoostrapSelectValue("id_locality", locality);
                // }
                setWidth('[data-id="id_locality"]');
            }
        };
        sendGetRequest(opts);
    }
}

function setBoostrapSelectValue(selectSelector, val) {
    //We need to show the text inside the span that the plugin show
    var selectBox = $("select#" + selectSelector);
    var text = selectBox.find("option[value='" + val + "']").text();
    selectBox.parent().find('.bootstrap-select .filter-option').text(text);
    //Check the selected attribute for the real select
    selectBox.val(val);
}

function addModal(type, options) {
    if (type == 1) {
        var body = '<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">  <div class="modal-header">    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>    <h3 id="myModalLabel"></h3>  </div>  <div class="modal-body"></div>  <div class="modal-footer">    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button></div></div>';
    }
    var def = {};
    $.extend(def, options);
    $("body").append(body);
    $("div#myModal #myModalLabel").html(options.title);
    $("div#myModal div.modal-body").html(options.body);
    $("#myModal").modal(def);

}

function addDatePicker(selector,options){
    var opts = {
        todayBtn: true,
        autoclose: true,
        format:"dd/mm/yy",
        orientation:"top left",
        startDate:new Date()
    };
    $.extend(opts, options);
    $(selector).datepicker(opts);
}


function verifyUser(){
    var userId = $("input#user_id").val();
    var otp = $("input#otp").val();
    var options = {
        url:"/verify_user/",
        type:"GET",
        success:function(data){
            if(data.status !="Success"){
                $("#signup-error").hide();
                $("#verify-error").html("The OTP you have entered is invalid.");
                $("#verify-error").show();
            }else{
                $("#signup-error").hide();
                $("#verify-error").html("Your account is now verified please login using your temporary password which was mailed to you.");
                $("#verify-error").show();
            }
        },
        data:{user_id:userId,pin:otp},
        dataType:"json"
    };
    $("#signup-error").hide();
    $("#verify-error").html("Verifying OTP please wait");
    $("#verify-error").show();
    sendGetRequest(options);
}

function toggleFavourite(elem,venue_id){
    var toggle ="add";
    if($(elem).hasClass("favourite")){
        toggle="remove";
        removeFavourite(venue_id);
    }else{
        toggle="add";
        setFavourite(venue_id);
    }

    var options={
        url:"/togglefavourites/",
        type:"GET",
        dataType:"json",
        data:{venue_id:venue_id,toggle:toggle},
        success:function(data){

        }
    };
    sendGetRequest(options);
}

function getFavourite(){
    var options = {
        url:"/getfavourites/",
        type:"GET",
        dataType:"json",
        success:function(data){
            for(var i =0;i<data.length;i++){
                setFavourite(data[i]);
            }
        }
    };

    sendGetRequest(options);
}

function setFavourite(venue_id){
    var parent = $("#"+venue_id+"-favourite");
    parent.removeClass("not-favourite");
    parent.addClass("favourite");
    parent.attr("src","/static/img/superplanner/favourite_selected.png");
}

function removeFavourite(venue_id){
    var parent = $("#"+venue_id+"-favourite");
    parent.removeClass("favourite");
    parent.addClass("not-favourite");
    parent.attr("src","/static/img/superplanner/favourite.png");
}


function showForgotPassword(){
    $("div.login").hide();
    $("div.forgot-password").show();
    $("#login-error").html("");
    $("#login-error").hide();
}

function showLogin(){
    $("div.login").show();
    $("div.forgot-password").hide();
    $("#fp-error").html("");
    $("#fp-error").hide();
}

function forgotPassword(){
    $("#fp-error").html("Your password is being retrieved please be patient");
    var options = {
        url:"/forgotpassword/",
        type:"POST",
        data:{username:$("input#fp-username").val()},
        dataType:"json",
        success:function(d){
            $("#fp-error").html("");
            $("#fp-error").html(d.message);
            $("#fp-error").show();
        }

    };
    sendPostRequest(options);
}

function setWidth(span){
    var chars= $(span).val();
    if(chars == ""){
        var tempspan = span+ " span";
        chars=$(tempspan).text().length;
        var width = chars * 10 + 28 ;
        $(span).css('width',width);
    }else{
    	chars = chars.length;
        var width = chars * 8+35;
        $(span).css('width',width);
    }
}

