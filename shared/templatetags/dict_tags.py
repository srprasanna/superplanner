from django import template

register = template.Library()

@register.filter(name='key_val')
def key_val(d,args):
    current_dict = d
    if args is None:
        return False
    arg_list = [arg.strip() for arg in args.split(',')]
    for arg in arg_list:
        current_dict = current_dict.get(arg,'')
    
    return current_dict
     
     
    
