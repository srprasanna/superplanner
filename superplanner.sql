-- MySQL dump 10.13  Distrib 5.5.29, for Win64 (x86)
--
-- Host: localhost    Database: superplanner
-- ------------------------------------------------------
-- Server version	5.5.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_5f412f9a` (`group_id`),
  KEY `auth_group_permissions_83d7f98b` (`permission_id`),
  CONSTRAINT `group_id_refs_id_f4b32aac` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `permission_id_refs_id_6ba0f519` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_37ef4eb4` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_d043b34a` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=142 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add permission',1,'add_permission'),(2,'Can change permission',1,'change_permission'),(3,'Can delete permission',1,'delete_permission'),(4,'Can add group',2,'add_group'),(5,'Can change group',2,'change_group'),(6,'Can delete group',2,'delete_group'),(7,'Can add user',3,'add_user'),(8,'Can change user',3,'change_user'),(9,'Can delete user',3,'delete_user'),(10,'Can add content type',4,'add_contenttype'),(11,'Can change content type',4,'change_contenttype'),(12,'Can delete content type',4,'delete_contenttype'),(13,'Can add session',5,'add_session'),(14,'Can change session',5,'change_session'),(15,'Can delete session',5,'delete_session'),(16,'Can add site',6,'add_site'),(17,'Can change site',6,'change_site'),(18,'Can delete site',6,'delete_site'),(19,'Can add log entry',7,'add_logentry'),(20,'Can change log entry',7,'change_logentry'),(21,'Can delete log entry',7,'delete_logentry'),(22,'Can add migration history',8,'add_migrationhistory'),(23,'Can change migration history',8,'change_migrationhistory'),(24,'Can delete migration history',8,'delete_migrationhistory'),(25,'Can add user profile',9,'add_userprofile'),(26,'Can change user profile',9,'change_userprofile'),(27,'Can delete user profile',9,'delete_userprofile'),(28,'Can add otp',10,'add_otp'),(29,'Can change otp',10,'change_otp'),(30,'Can delete otp',10,'delete_otp'),(31,'Can add event type',11,'add_eventtype'),(32,'Can change event type',11,'change_eventtype'),(33,'Can delete event type',11,'delete_eventtype'),(34,'Can add event sub type',12,'add_eventsubtype'),(35,'Can change event sub type',12,'change_eventsubtype'),(36,'Can delete event sub type',12,'delete_eventsubtype'),(37,'Can add country',13,'add_country'),(38,'Can change country',13,'change_country'),(39,'Can delete country',13,'delete_country'),(40,'Can add state',14,'add_state'),(41,'Can change state',14,'change_state'),(42,'Can delete state',14,'delete_state'),(43,'Can add city',15,'add_city'),(44,'Can change city',15,'change_city'),(45,'Can delete city',15,'delete_city'),(46,'Can add location',16,'add_location'),(47,'Can change location',16,'change_location'),(48,'Can delete location',16,'delete_location'),(49,'Can add seating option',17,'add_seatingoption'),(50,'Can change seating option',17,'change_seatingoption'),(51,'Can delete seating option',17,'delete_seatingoption'),(52,'Can add venue type',18,'add_venuetype'),(53,'Can change venue type',18,'change_venuetype'),(54,'Can delete venue type',18,'delete_venuetype'),(55,'Can add capacity range',19,'add_capacityrange'),(56,'Can change capacity range',19,'change_capacityrange'),(57,'Can delete capacity range',19,'delete_capacityrange'),(58,'Can add meal type',20,'add_mealtype'),(59,'Can change meal type',20,'change_mealtype'),(60,'Can delete meal type',20,'delete_mealtype'),(73,'Can add venue',25,'add_venue'),(74,'Can change venue',25,'change_venue'),(75,'Can delete venue',25,'delete_venue'),(76,'Can add venue rating',26,'add_venuerating'),(77,'Can change venue rating',26,'change_venuerating'),(78,'Can delete venue rating',26,'delete_venuerating'),(79,'Can add custom option',27,'add_customoption'),(80,'Can change custom option',27,'change_customoption'),(81,'Can delete custom option',27,'delete_customoption'),(82,'Can add venue supports event type',28,'add_venuesupportseventtype'),(83,'Can change venue supports event type',28,'change_venuesupportseventtype'),(84,'Can delete venue supports event type',28,'delete_venuesupportseventtype'),(85,'Can add venue picture',29,'add_venuepicture'),(86,'Can change venue picture',29,'change_venuepicture'),(87,'Can delete venue picture',29,'delete_venuepicture'),(94,'Can add venue booking history',32,'add_venuebookinghistory'),(95,'Can change venue booking history',32,'change_venuebookinghistory'),(96,'Can delete venue booking history',32,'delete_venuebookinghistory'),(97,'Can add quote',33,'add_quote'),(98,'Can change quote',33,'change_quote'),(99,'Can delete quote',33,'delete_quote'),(100,'Can add user social auth',34,'add_usersocialauth'),(101,'Can change user social auth',34,'change_usersocialauth'),(102,'Can delete user social auth',34,'delete_usersocialauth'),(103,'Can add nonce',35,'add_nonce'),(104,'Can change nonce',35,'change_nonce'),(105,'Can delete nonce',35,'delete_nonce'),(106,'Can add association',36,'add_association'),(107,'Can change association',36,'change_association'),(108,'Can delete association',36,'delete_association'),(109,'Can add meal category',37,'add_mealcategory'),(110,'Can change meal category',37,'change_mealcategory'),(111,'Can delete meal category',37,'delete_mealcategory'),(112,'Can add meal item',38,'add_mealitem'),(113,'Can change meal item',38,'change_mealitem'),(114,'Can delete meal item',38,'delete_mealitem'),(115,'Can add meal option',39,'add_mealoption'),(116,'Can change meal option',39,'change_mealoption'),(117,'Can delete meal option',39,'delete_mealoption'),(121,'Can add package',41,'add_package'),(122,'Can change package',41,'change_package'),(123,'Can delete package',41,'delete_package'),(124,'Can add package description',42,'add_packagedescription'),(125,'Can change package description',42,'change_packagedescription'),(126,'Can delete package description',42,'delete_packagedescription'),(127,'Can add meal category item',43,'add_mealcategoryitem'),(128,'Can change meal category item',43,'change_mealcategoryitem'),(129,'Can delete meal category item',43,'delete_mealcategoryitem'),(130,'Can add meal category item option',44,'add_mealcategoryitemoption'),(131,'Can change meal category item option',44,'change_mealcategoryitemoption'),(132,'Can delete meal category item option',44,'delete_mealcategoryitemoption'),(133,'Can add quote venue',45,'add_quotevenue'),(134,'Can change quote venue',45,'change_quotevenue'),(135,'Can delete quote venue',45,'delete_quotevenue'),(136,'Can add package contents',46,'add_packagecontents'),(137,'Can change package contents',46,'change_packagecontents'),(138,'Can delete package contents',46,'delete_packagecontents'),(139,'Can add contact',47,'add_contact'),(140,'Can change contact',47,'change_contact'),(141,'Can delete contact',47,'delete_contact');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$10000$QhkpLrtSnh44$mr9ke1M36k8xAhS1hMYfnUrfpFSAfJpv+oWnBGXnujI=','2013-08-30 09:54:12',1,'prasanna','','','prasanna@beehyv.com',1,1,'2013-07-31 14:14:50'),(2,'!','2013-08-01 05:57:55',0,'prasanna@srprasanna.com','Prasanna','Sambandam Raghu','prasanna@srprasanna.com',0,1,'2013-08-01 05:02:41'),(3,'!','2013-08-09 12:41:34',0,'srprasanna','Prasanna','S R','',0,1,'2013-08-01 05:03:28');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_6340c63c` (`user_id`),
  KEY `auth_user_groups_5f412f9a` (`group_id`),
  CONSTRAINT `group_id_refs_id_274b862c` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `user_id_refs_id_40c41112` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_6340c63c` (`user_id`),
  KEY `auth_user_user_permissions_83d7f98b` (`permission_id`),
  CONSTRAINT `permission_id_refs_id_35d9ac25` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `user_id_refs_id_4dc23c39` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_6340c63c` (`user_id`),
  KEY `django_admin_log_37ef4eb4` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_93d2d1f8` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `user_id_refs_id_c0d12874` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2013-07-18 08:17:23',1,29,'1','VenuePicture object',1,''),(2,'2013-07-18 08:22:52',1,29,'2','VenuePicture object',1,''),(3,'2013-07-30 04:09:34',1,29,'3','VenuePicture object',1,''),(4,'2013-07-30 04:09:49',1,29,'4','VenuePicture object',1,''),(5,'2013-07-30 06:45:03',1,15,'2','Visakhapatnam',1,''),(6,'2013-07-30 06:45:39',1,13,'1','India',2,'Added state \"Karnataka\".'),(7,'2013-07-30 06:46:10',1,15,'3','Bangalore',1,''),(8,'2013-08-29 12:04:21',1,11,'3','Celebration',1,''),(9,'2013-08-30 03:35:40',1,11,'3','Celebration',3,''),(10,'2013-08-30 03:35:40',1,11,'2','Marriage',3,''),(11,'2013-08-30 03:35:40',1,11,'1','Conference',3,''),(12,'2013-08-30 03:35:55',1,11,'4','All',1,''),(13,'2013-08-30 03:36:14',1,11,'5','Weddings',1,''),(14,'2013-08-30 03:36:21',1,11,'6','Cocktail Parties',1,''),(15,'2013-08-30 03:36:25',1,11,'7','Seminars',1,''),(16,'2013-08-30 03:36:44',1,11,'8','Board Meetings',1,''),(17,'2013-08-30 03:36:49',1,11,'9','Concerts',1,''),(18,'2013-08-30 03:37:22',1,11,'10','Exhibitions/Trade Fairs',1,''),(19,'2013-08-30 03:37:29',1,11,'11','Kiddie Parties',1,''),(20,'2013-08-30 03:38:46',1,25,'2','Wild Ginger',3,''),(21,'2013-08-30 03:38:46',1,25,'1','Purple Leaf Hotel',3,''),(22,'2013-08-30 03:55:32',1,25,'3','The Purple Leaf Hotel',1,''),(23,'2013-08-30 04:03:12',1,25,'3','The Purple Leaf Hotel',3,''),(24,'2013-08-30 04:08:47',1,25,'7','The Purple Leaf Hotel',1,''),(25,'2013-08-30 04:10:34',1,25,'8','Hotel1',1,''),(26,'2013-08-30 04:15:30',1,25,'9','Hotel2',1,''),(27,'2013-08-30 04:16:47',1,25,'10','Hotel3',1,''),(28,'2013-08-30 04:19:15',1,25,'11','Hotel4',1,''),(29,'2013-08-30 04:20:14',1,25,'12','Hotel5',1,''),(30,'2013-08-30 04:22:03',1,25,'13','Hotel6',1,''),(31,'2013-08-30 04:23:18',1,25,'14','Hotel7',1,''),(32,'2013-08-30 04:23:55',1,25,'15','Hotel8',1,''),(33,'2013-08-30 09:54:36',1,15,'2','Visakhapatnam',3,''),(34,'2013-08-30 09:55:19',1,15,'1','Hyderabad',2,'Deleted location \"Karkhana, Hyderabad\".'),(35,'2013-08-30 09:56:01',1,15,'3','Bangalore',2,'Added location \"M G Road, Bangalore\".'),(36,'2013-08-30 09:57:22',1,25,'16','Hotel9',1,''),(37,'2013-08-30 09:58:07',1,25,'17','Hotel10',1,''),(38,'2013-08-30 09:58:31',1,25,'17','Hotel10',2,'Added venue picture \"Hotel10\". Added venue picture \"Hotel10\".'),(39,'2013-08-30 09:59:19',1,25,'18','Hotel11',1,'');
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'permission','auth','permission'),(2,'group','auth','group'),(3,'user','auth','user'),(4,'content type','contenttypes','contenttype'),(5,'session','sessions','session'),(6,'site','sites','site'),(7,'log entry','admin','logentry'),(8,'migration history','south','migrationhistory'),(9,'user profile','venueregistration','userprofile'),(10,'otp','venueregistration','otp'),(11,'event type','venueregistration','eventtype'),(12,'event sub type','venueregistration','eventsubtype'),(13,'country','venueregistration','country'),(14,'state','venueregistration','state'),(15,'city','venueregistration','city'),(16,'location','venueregistration','location'),(17,'seating option','venueregistration','seatingoption'),(18,'venue type','venueregistration','venuetype'),(19,'capacity range','venueregistration','capacityrange'),(20,'meal type','venueregistration','mealtype'),(25,'venue','venueregistration','venue'),(26,'venue rating','venueregistration','venuerating'),(27,'custom option','venueregistration','customoption'),(28,'venue supports event type','venueregistration','venuesupportseventtype'),(29,'venue picture','venueregistration','venuepicture'),(32,'venue booking history','venueregistration','venuebookinghistory'),(33,'quote','venueregistration','quote'),(34,'user social auth','social_auth','usersocialauth'),(35,'nonce','social_auth','nonce'),(36,'association','social_auth','association'),(37,'meal category','venueregistration','mealcategory'),(38,'meal item','venueregistration','mealitem'),(39,'meal option','venueregistration','mealoption'),(41,'package','venueregistration','package'),(42,'package description','venueregistration','packagedescription'),(43,'meal category item','venueregistration','mealcategoryitem'),(44,'meal category item option','venueregistration','mealcategoryitemoption'),(45,'quote venue','venueregistration','quotevenue'),(46,'package contents','venueregistration','packagecontents'),(47,'contact','venueregistration','contact');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_b7b81f0c` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('16ot5r3ox0efiamo7cpb6d098h7jn7jp','NWY2NDRlZWNlNTdjNjQwY2RkMDAzZjBhYzQ3YjQ2YzNmNDRiOTQ0ZDqAAn1xAShVB3NlYXRpbmdYAQAAADFVCmV2ZW50X3R5cGVYAQAAADFVC2Vudmlyb25tZW50WAEAAAAxVQ5zZWFyY2hfcmVxdWVzdFhoAAAAL3NlYXJjaC8/Y2l0eT0xJmxvY2FsaXR5PTEmb2NjYXNpb249MSZkYXRlPTI5JTJGMDglMkYxMyZ0aW1lPTA2JTNBMjUrUE0mZ3Vlc3RzPTEmc2VhdGluZz0xJmVudmlyb25tZW50PTFVBmd1ZXN0c1gBAAAAMVUEdGltZVgIAAAAMDY6MjUgUE1xAlUEZGF0ZVUKMjAxMy0wOC0yOVUIb2NjYXNpb25YAQAAADF1Lg==','2013-09-07 14:00:12'),('6dlq12978rhcpshara854atgz5itn1lg','NDIwOTQ0ZmMwMTRkYjllMTI0ZjlkZmVhY2JiYmEzNmJlMzZjMjdmOTqAAn1xAShVB3NlYXRpbmdYAQAAADFVCmV2ZW50X3R5cGVYAQAAADFVDV9hdXRoX3VzZXJfaWSKAQFVEl9hdXRoX3VzZXJfYmFja2VuZFUpZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmRVC2Vudmlyb25tZW50WAEAAAAxVQ5zZWFyY2hfcmVxdWVzdFhmAAAAL3NlYXJjaC8/Y2l0eT0xJmxvY2FsaXR5PTEmb2NjYXNpb249MSZkYXRlPTIwMTMtMDgtMjImdGltZT0wNCUzQTI1K1BNJmd1ZXN0cz0xJnNlYXRpbmc9MSZlbnZpcm9ubWVudD0xVQZndWVzdHNYAQAAADFVBHRpbWVYCAAAADA0OjI1IFBNcQJVBGRhdGVYCgAAADIwMTMtMDgtMjJxA1UIb2NjYXNpb25YAQAAADF1Lg==','2013-09-04 10:58:35'),('ac5ibxjzx1doowwrr0pnhb9i79jap484','Y2RmMWY4N2RmMzlkZWVjNjU2MjQ2YzE5ZjE1NGI3ZGIzNTRmYmQzYjqAAn1xAShVDnNlYXJjaF9yZXF1ZXN0WBoAAAAvdmVudWVyZWdpc3RyYXRpb24vc2VhcmNoL3ECVQ1fYXV0aF91c2VyX2lkigEBVRJfYXV0aF91c2VyX2JhY2tlbmRVKWRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kVQpldmVudF90eXBlWAEAAAAxdS4=','2013-08-05 13:01:27'),('adkdrlytp84oj1xm6ovavctf6vxm7lps','NTAyYjQzOTQyZjMyNjYzNGQ1YmJlMGQ1Zjc0NWI5YjgzY2ViN2FkOTqAAn1xAShVDnNlYXJjaF9yZXF1ZXN0WBoAAAAvdmVudWVyZWdpc3RyYXRpb24vc2VhcmNoL3ECVQpldmVudF90eXBlSwF1Lg==','2013-08-16 09:35:28'),('d2cr6dwm022gq9np54knr5d3qsfkze08','YTEzOGE1NDYwNWViOGU2Mjg4ZDBlMTY3ZjRlMzczMmE2MzYwZWUwYjqAAn1xAShVDnNlYXJjaF9yZXF1ZXN0WBoAAAAvdmVudWVyZWdpc3RyYXRpb24vc2VhcmNoL3ECVRJfYXV0aF91c2VyX2JhY2tlbmRVKWRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kVQ1fYXV0aF91c2VyX2lkigEBVQpldmVudF90eXBlWAEAAAAxdS4=','2013-08-13 04:30:37'),('et0ngt12hbq4hlxephy1in70uiwrb9u0','OWIyZGJlMjEwNTdlN2MyOTA1MGU3YTU0MTJiOWZkMTRlYzE0M2Q2YjqAAn1xAShVB3NlYXRpbmdYAQAAADFVHnR3aXR0ZXJ1bmF1dGhvcml6ZWRfdG9rZW5fbmFtZV1xAlWUb2F1dGhfdG9rZW5fc2VjcmV0PVVhMVZudG9YYlZobE5RZERZdjRFb1FnM1ZhQ01TVkYzT1Q1TXNRVlpOWVkmb2F1dGhfdG9rZW49Rmx1MW9Ia053NUJ6TWZHTFJacDNhZWkzU3R6ZmV6RFp4aUhtcXo4MFB6byZvYXV0aF9jYWxsYmFja19jb25maXJtZWQ9dHJ1ZXEDYVUKZXZlbnRfdHlwZUsBVQtlbnZpcm9ubWVudFgBAAAAMVUOc2VhcmNoX3JlcXVlc3RYaAAAAC9zZWFyY2gvP2NpdHk9MSZsb2NhbGl0eT0xJm9jY2FzaW9uPTEmZGF0ZT1kZCUyRm1tJTJGeXkmdGltZT0wOCUzQTI1K0FNJmd1ZXN0cz0xJnNlYXRpbmc9MSZlbnZpcm9ubWVudD0xVQZndWVzdHNYAQAAADFVBHRpbWVYCAAAADA4OjI1IEFNcQRVBGRhdGVVCjIwMTMtMDgtMzBVCG9jY2FzaW9uSwF1Lg==','2013-09-13 09:50:31'),('fw08bpi79do4qk3x46vhsmwrrmz38q8t','YTBkZTBlYzc3Mzk3MTRhYWEzYTA2NjNlMTMxNWNkYjk3YTM2Mzc5ZjqAAn1xAShVB3NlYXRpbmdYAQAAADFVCmV2ZW50X3R5cGVYAQAAADFVC2Vudmlyb25tZW50WAEAAAAxVQ5zZWFyY2hfcmVxdWVzdFhoAAAAL3NlYXJjaC8/Y2l0eT0xJmxvY2FsaXR5PTEmb2NjYXNpb249MSZkYXRlPWRkJTJGbW0lMkZ5eSZ0aW1lPTA2JTNBNDUrUE0mZ3Vlc3RzPTEmc2VhdGluZz0xJmVudmlyb25tZW50PTFVBmd1ZXN0c1gBAAAAMVUEdGltZVgIAAAAMDY6NDUgUE1xAlUEZGF0ZVUKMjAxMy0wOC0yNlUIb2NjYXNpb25YAQAAADF1Lg==','2013-09-09 13:19:02'),('hjrpn337hkv597t5y7eef5hdzhifx19h','NTRmMGI0MThmMGU0YmJmMWEyMzMyMjViN2YwOThiNjIxMzU1ODA5NTqAAn1xAShVDnNlYXJjaF9yZXF1ZXN0WBoAAAAvdmVudWVyZWdpc3RyYXRpb24vc2VhcmNoL3ECVQpldmVudF90eXBlWAEAAAAxdS4=','2013-08-19 10:55:08'),('mf4k34d8kcufolrca3zp9fa0xyylxmd7','MWE2MDBmZmI2Y2ZmMzdmYjFiMzBhYmQ4NTc4MTg4ODU2MDFlMDgwNDqAAn1xAShVB3NlYXRpbmdYAQAAADFVCmV2ZW50X3R5cGVYAQAAADRVDV9hdXRoX3VzZXJfaWSKAQFVEl9hdXRoX3VzZXJfYmFja2VuZFUpZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmRVC2Vudmlyb25tZW50WAEAAAAxVQ5zZWFyY2hfcmVxdWVzdFhoAAAAL3NlYXJjaC8/Y2l0eT0yJmxvY2FsaXR5PTQmb2NjYXNpb249NCZkYXRlPWRkJTJGbW0lMkZ5eSZ0aW1lPTAxJTNBNTUrUE0mZ3Vlc3RzPTEmc2VhdGluZz0xJmVudmlyb25tZW50PTFVBmd1ZXN0c1gBAAAAMVUEdGltZVgIAAAAMDE6NTUgUE1xAlUEZGF0ZVUKMjAxMy0wOC0zMFUIb2NjYXNpb25YAQAAADR1Lg==','2013-09-13 08:40:46'),('p25n40qjhato3tbe2peq9i6dzspd65g7','YjFiNTY2Mzc5NzI3Y2I3MzliNTA2ZWZmMWZmMTE1YTZiNjUyZjFhNTqAAn1xAShVB3NlYXRpbmdYAQAAADFVCmV2ZW50X3R5cGVLAVUNX2F1dGhfdXNlcl9pZIoBAVUSX2F1dGhfdXNlcl9iYWNrZW5kVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZFULZW52aXJvbm1lbnRYAQAAADFVDnNlYXJjaF9yZXF1ZXN0WGoAAAAvc2VhcmNoLz9jaXR5PTEmbG9jYWxpdHk9MSZvY2Nhc2lvbj0xJmRhdGU9MDglMkYwOCUyRjIwMTMmdGltZT0wNiUzQTAwK1BNJmd1ZXN0cz0xJnNlYXRpbmc9MSZlbnZpcm9ubWVudD0xVQZndWVzdHNYAQAAADFVBHRpbWVYCAAAADA2OjAwIFBNcQJVBGRhdGVVCjIwMTMtMDgtMzBVCG9jY2FzaW9uSwF1Lg==','2013-09-13 09:49:58'),('soeb9ilmimw79ie3h62wsayp80kwggwt','Njg0ZWM4ZWE5MGRjMGI4ZTRmMjU1OTU1NmFjNDE4YzUyYzU5NjQ4NzqAAn1xAShVDmZhY2Vib29rX3N0YXRlWCAAAABsaTFGMEdjWDFaaGQ0RDZqQ2pMWGFzTWlhT0FndU5LWlUOc2VhcmNoX3JlcXVlc3RYGgAAAC92ZW51ZXJlZ2lzdHJhdGlvbi9zZWFyY2gvcQJVEl9hdXRoX3VzZXJfYmFja2VuZFUpZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmRVDV9hdXRoX3VzZXJfaWSKAQFVCmV2ZW50X3R5cGVLAXUu','2013-08-13 06:48:39'),('sp6u3ks6a2iaq5hxioosabilxbi2x2cm','ZmExYmI3NTAzZjM3YmM4NzFjMjUyM2Q0OGQwYTE2NGZlOGFiNWZjZjqAAn1xAShVB3NlYXRpbmdYAQAAADFVCmV2ZW50X3R5cGVYAQAAADFVDV9hdXRoX3VzZXJfaWSKAQFVB2ZfYW5kX2KIVRJfYXV0aF91c2VyX2JhY2tlbmRVKWRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kVQtlbnZpcm9ubWVudFgBAAAAMVUOc2VhcmNoX3JlcXVlc3RYZgAAAC9zZWFyY2gvP2NpdHk9MSZsb2NhbGl0eT0xJm9jY2FzaW9uPTEmZGF0ZT0yMDEzLTA4LTIyJnRpbWU9MDYlM0EzNStQTSZndWVzdHM9MSZzZWF0aW5nPTEmZW52aXJvbm1lbnQ9MVUGZ3Vlc3RzWAEAAAAxVQR0aW1lWAgAAAAwNjozNSBQTXECVQRkYXRlWAoAAAAyMDEzLTA4LTIycQNVCG9jY2FzaW9uWAEAAAAxdS4=','2013-09-05 13:08:37'),('yww7i1fn4b8j507xb69q2z2n2sx3wddg','ZWJiMjEyMzA3ZWFiYjI0MGZmZTc1MzM4MTcxMjc5YTUyMDY5YjE5YTqAAn1xAShVB3NlYXRpbmdYAQAAADFVCmV2ZW50X3R5cGVLAFUNX2F1dGhfdXNlcl9pZIoBAVUSX2F1dGhfdXNlcl9iYWNrZW5kVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZFULZW52aXJvbm1lbnRYAQAAADFVDnNlYXJjaF9yZXF1ZXN0WF0AAAAvc2VhcmNoLz9jaXR5PTEmbG9jYWxpdHk9MSZkYXRlPTMwJTJGMDglMkYxMyZ0aW1lPTAzJTNBMTcrUE0mZ3Vlc3RzPTEmc2VhdGluZz0xJmVudmlyb25tZW50PTFVBmd1ZXN0c1gBAAAAMVUJX21lc3NhZ2VzXXECKGNkamFuZ28uY29udHJpYi5tZXNzYWdlcy5zdG9yYWdlLmJhc2UKTWVzc2FnZQpxAymBcQR9cQUoVQpleHRyYV90YWdzcQZOVQdtZXNzYWdlcQdYHAAAAFN1Y2Nlc3NmdWxseSBkZWxldGVkIDEgY2l0eS5VBWxldmVscQhLFHViaAMpgXEJfXEKKGgGWAAAAABoB1guAAAAVGhlIGNpdHkgIkh5ZGVyYWJhZCIgd2FzIGNoYW5nZWQgc3VjY2Vzc2Z1bGx5LmgISxR1YmgDKYFxC31xDChoBlgAAAAAaAdYLgAAAFRoZSBjaXR5ICJCYW5nYWxvcmUiIHdhcyBjaGFuZ2VkIHN1Y2Nlc3NmdWxseS5oCEsUdWJoAymBcQ19cQ4oaAZYAAAAAGgHWEsAAABUaGUgdmVudWUgIkhvdGVsOSIgd2FzIGFkZGVkIHN1Y2Nlc3NmdWxseS4gWW91IG1heSBhZGQgYW5vdGhlciB2ZW51ZSBiZWxvdy5oCEsUdWJoAymBcQ99cRAoaAZYAAAAAGgHWEwAAABUaGUgdmVudWUgIkhvdGVsMTAiIHdhcyBhZGRlZCBzdWNjZXNzZnVsbHkuIFlvdSBtYXkgYWRkIGFub3RoZXIgdmVudWUgYmVsb3cuaAhLFHViaAMpgXERfXESKGgGWAAAAABoB1gtAAAAVGhlIHZlbnVlICJIb3RlbDEwIiB3YXMgY2hhbmdlZCBzdWNjZXNzZnVsbHkuaAhLFHViaAMpgXETfXEUKGgGWAAAAABoB1hMAAAAVGhlIHZlbnVlICJIb3RlbDExIiB3YXMgYWRkZWQgc3VjY2Vzc2Z1bGx5LiBZb3UgbWF5IGFkZCBhbm90aGVyIHZlbnVlIGJlbG93LmgISxR1YmVVBHRpbWVYCAAAADAzOjE3IFBNVQRkYXRlVQoyMDEzLTA4LTMwVQhvY2Nhc2lvbksAdS4=','2013-09-13 09:59:19'),('zaplhd1op2h7d7qumlszlu4hgo8muz8i','OTY3MmIxNDJlYTk5NmEyZDJlNGFjNzZjMmVmNmNmZjljNDdiMzk1MzqAAn1xAShVDnNlYXJjaF9yZXF1ZXN0WAgAAAAvc2VhcmNoL3ECVQpldmVudF90eXBlWAEAAAAxdS4=','2013-08-30 09:03:00');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_site`
--

DROP TABLE IF EXISTS `django_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_site`
--

LOCK TABLES `django_site` WRITE;
/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;
INSERT INTO `django_site` VALUES (1,'example.com','example.com');
/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_auth_association`
--

DROP TABLE IF EXISTS `social_auth_association`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_auth_association` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server_url` varchar(16) NOT NULL,
  `handle` varchar(16) NOT NULL,
  `secret` varchar(255) NOT NULL,
  `issued` int(11) NOT NULL,
  `lifetime` int(11) NOT NULL,
  `assoc_type` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `social_auth_association_handle_207fa6ae_uniq` (`handle`,`server_url`),
  KEY `social_auth_association_28e32fb0` (`issued`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_auth_association`
--

LOCK TABLES `social_auth_association` WRITE;
/*!40000 ALTER TABLE `social_auth_association` DISABLE KEYS */;
/*!40000 ALTER TABLE `social_auth_association` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_auth_nonce`
--

DROP TABLE IF EXISTS `social_auth_nonce`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_auth_nonce` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server_url` varchar(16) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `salt` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `social_auth_nonce_timestamp_10adadb6_uniq` (`timestamp`,`salt`,`server_url`),
  KEY `social_auth_nonce_d7e6d55b` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_auth_nonce`
--

LOCK TABLES `social_auth_nonce` WRITE;
/*!40000 ALTER TABLE `social_auth_nonce` DISABLE KEYS */;
/*!40000 ALTER TABLE `social_auth_nonce` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_auth_usersocialauth`
--

DROP TABLE IF EXISTS `social_auth_usersocialauth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_auth_usersocialauth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `provider` varchar(32) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `extra_data` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `social_auth_usersocialauth_provider_1d3b5e05_uniq` (`provider`,`uid`),
  KEY `social_auth_usersocialauth_6340c63c` (`user_id`),
  CONSTRAINT `user_id_refs_id_e6cbdf29` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_auth_usersocialauth`
--

LOCK TABLES `social_auth_usersocialauth` WRITE;
/*!40000 ALTER TABLE `social_auth_usersocialauth` DISABLE KEYS */;
INSERT INTO `social_auth_usersocialauth` VALUES (1,2,'facebook','576320905','{\"access_token\": \"CAAH4cA3HjtQBANbShRnAMU4gq3Spk9j1YRhgHUUXDcNSyrmosGfTGR8Yr4UEP75dQ6e9Xw9KQTZCx6WpwIY5YogeWCmrkIzBlDuLv0DfDLSzPqQWigaykQhKS9q1zFbyJfElQTZCDdMToEcgof\", \"expires\": \"5180684\", \"id\": \"576320905\"}'),(2,3,'twitter','19170353','{\"access_token\": \"oauth_token_secret=ZdvOqVImQzCv6AvFMeb8N1qX0Gc4rv6RDnVwzO9qORM&oauth_token=19170353-dxbHSGEX2fxyjiV48dxUrfzER9c3IEN7kJTz92KCq\", \"id\": 19170353, \"screen_name\": \"srprasanna\"}');
/*!40000 ALTER TABLE `social_auth_usersocialauth` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `south_migrationhistory`
--

DROP TABLE IF EXISTS `south_migrationhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `south_migrationhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(255) NOT NULL,
  `migration` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `south_migrationhistory`
--

LOCK TABLES `south_migrationhistory` WRITE;
/*!40000 ALTER TABLE `south_migrationhistory` DISABLE KEYS */;
INSERT INTO `south_migrationhistory` VALUES (1,'venueregistration','0001_initial','2013-07-31 14:15:14'),(2,'venueregistration','0002_auto__add_field_venue_base_package_rate','2013-07-31 14:15:15'),(3,'venueregistration','0003_auto__add_field_venue_address','2013-07-31 14:15:15'),(4,'venueregistration','0004_auto__del_field_venue_address__add_field_venue_address1__add_field_ven','2013-07-31 14:15:15'),(5,'social_auth','0001_initial','2013-07-31 14:15:17'),(6,'social_auth','0002_auto__add_unique_nonce_timestamp_salt_server_url__add_unique_associati','2013-07-31 14:15:17'),(7,'venueregistration','0005_auto__del_mealpackage__del_venuepackage__del_mealcategorypackage__del_','2013-08-21 06:28:31'),(8,'venueregistration','0006_auto__add_field_mealoption_name__add_field_mealitem_name','2013-08-21 06:29:48'),(9,'venueregistration','0007_auto__add_field_mealtype_name','2013-08-21 06:31:56'),(10,'venueregistration','0008_auto__del_mealitemoption__add_mealcategoryitemoption__add_mealcategory','2013-08-21 06:43:57'),(11,'venueregistration','0009_auto__add_field_mealcategory_how_to','2013-08-21 07:20:19'),(12,'venueregistration','0005_auto__add_field_userprofile_is_verified','2013-08-05 00:00:00'),(13,'venueregistration','0010_auto__chg_field_quote_time_of_booking','2013-08-21 08:39:39'),(14,'venueregistration','0011_auto__add_quotevenue','2013-08-21 08:42:14'),(15,'venueregistration','0012_auto__add_packagecontents','2013-08-21 08:49:35'),(16,'venueregistration','0013_auto__chg_field_quote_date_of_booking','2013-08-21 09:45:45'),(17,'venueregistration','0014_auto__del_field_quote_updated_at__del_field_quote_created_at','2013-08-21 10:42:27'),(18,'venueregistration','0015_auto__del_field_quote_time_of_booking','2013-08-21 10:56:40'),(19,'venueregistration','0016_auto__add_field_package_meal_type','2013-08-21 12:16:50'),(20,'venueregistration','0017_auto','2013-08-22 05:59:32'),(21,'venueregistration','0018_auto__add_field_venuerating_review','2013-08-22 09:42:14'),(22,'venueregistration','0019_auto__add_field_venuerating_created_at','2013-08-22 09:56:57'),(23,'venueregistration','0020_auto__add_contact','2013-08-24 06:38:40'),(24,'venueregistration','0021_auto__chg_field_contact_mobile','2013-08-24 07:41:45');
/*!40000 ALTER TABLE `south_migrationhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_capacityrange`
--

DROP TABLE IF EXISTS `venueregistration_capacityrange`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_capacityrange` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `minimum` int(11) NOT NULL,
  `maximum` int(11) DEFAULT NULL,
  `display_string` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_capacityrange`
--

LOCK TABLES `venueregistration_capacityrange` WRITE;
/*!40000 ALTER TABLE `venueregistration_capacityrange` DISABLE KEYS */;
INSERT INTO `venueregistration_capacityrange` VALUES (1,0,10,'0-10'),(2,11,50,'11-50');
/*!40000 ALTER TABLE `venueregistration_capacityrange` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_city`
--

DROP TABLE IF EXISTS `venueregistration_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `state_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `venueregistration_city_5654bf12` (`state_id`),
  CONSTRAINT `state_id_refs_id_2cad718d` FOREIGN KEY (`state_id`) REFERENCES `venueregistration_state` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_city`
--

LOCK TABLES `venueregistration_city` WRITE;
/*!40000 ALTER TABLE `venueregistration_city` DISABLE KEYS */;
INSERT INTO `venueregistration_city` VALUES (1,'Hyderabad',1),(3,'Bangalore',2);
/*!40000 ALTER TABLE `venueregistration_city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_contact`
--

DROP TABLE IF EXISTS `venueregistration_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` bigint(20),
  `company` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_contact`
--

LOCK TABLES `venueregistration_contact` WRITE;
/*!40000 ALTER TABLE `venueregistration_contact` DISABLE KEYS */;
INSERT INTO `venueregistration_contact` VALUES (1,'Prasanna S R','prasanna@beehyv.com',9676589044,'Beehyv Software Solutions');
/*!40000 ALTER TABLE `venueregistration_contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_country`
--

DROP TABLE IF EXISTS `venueregistration_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_country`
--

LOCK TABLES `venueregistration_country` WRITE;
/*!40000 ALTER TABLE `venueregistration_country` DISABLE KEYS */;
INSERT INTO `venueregistration_country` VALUES (1,'India');
/*!40000 ALTER TABLE `venueregistration_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_customoption`
--

DROP TABLE IF EXISTS `venueregistration_customoption`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_customoption` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `venue_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `venueregistration_customoption_03eb0938` (`venue_id`),
  CONSTRAINT `venue_id_refs_id_f6458eca` FOREIGN KEY (`venue_id`) REFERENCES `venueregistration_venue` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_customoption`
--

LOCK TABLES `venueregistration_customoption` WRITE;
/*!40000 ALTER TABLE `venueregistration_customoption` DISABLE KEYS */;
/*!40000 ALTER TABLE `venueregistration_customoption` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_eventsubtype`
--

DROP TABLE IF EXISTS `venueregistration_eventsubtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_eventsubtype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `event_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `venueregistration_eventsubtype_3120f0b2` (`event_type_id`),
  CONSTRAINT `event_type_id_refs_id_152853b6` FOREIGN KEY (`event_type_id`) REFERENCES `venueregistration_eventtype` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_eventsubtype`
--

LOCK TABLES `venueregistration_eventsubtype` WRITE;
/*!40000 ALTER TABLE `venueregistration_eventsubtype` DISABLE KEYS */;
/*!40000 ALTER TABLE `venueregistration_eventsubtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_eventtype`
--

DROP TABLE IF EXISTS `venueregistration_eventtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_eventtype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_eventtype`
--

LOCK TABLES `venueregistration_eventtype` WRITE;
/*!40000 ALTER TABLE `venueregistration_eventtype` DISABLE KEYS */;
INSERT INTO `venueregistration_eventtype` VALUES (4,'All'),(5,'Weddings'),(6,'Cocktail Parties'),(7,'Seminars'),(8,'Board Meetings'),(9,'Concerts'),(10,'Exhibitions/Trade Fairs'),(11,'Kiddie Parties');
/*!40000 ALTER TABLE `venueregistration_eventtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_location`
--

DROP TABLE IF EXISTS `venueregistration_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `city_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `venueregistration_location_b376980e` (`city_id`),
  CONSTRAINT `city_id_refs_id_568bc93b` FOREIGN KEY (`city_id`) REFERENCES `venueregistration_city` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_location`
--

LOCK TABLES `venueregistration_location` WRITE;
/*!40000 ALTER TABLE `venueregistration_location` DISABLE KEYS */;
INSERT INTO `venueregistration_location` VALUES (1,'Trimulgherry',1),(2,'Banjara Hills',1),(6,'Jeyanagar',3),(7,'M G Road',3);
/*!40000 ALTER TABLE `venueregistration_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_mealcategory`
--

DROP TABLE IF EXISTS `venueregistration_mealcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_mealcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `how_to` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_mealcategory`
--

LOCK TABLES `venueregistration_mealcategory` WRITE;
/*!40000 ALTER TABLE `venueregistration_mealcategory` DISABLE KEYS */;
INSERT INTO `venueregistration_mealcategory` VALUES (1,'Appetizers/Snacks','<p>You should carefully decide the time , the appetizers are \"circulated\" for as this determines the cost. If your party begins at 8PM and you expect to serve dinner at 10, then choose the 120 minutes package.</p><p>Also do note that pricing for non-veg appetizers quoted are for boneless meat.'),(2,'Soups','<p>Normally soups are served clear, or with stock. You can select your soups now, and you can indicate your preference upon finalizing the menu with the venue.</p>'),(3,'Salads',NULL),(4,'Main Course','<p>All packages include a choice of Rice, Indian Breads and Dahi/Raitas. If your selection is a Continental/Chinese menu, the breads and dahi shall be suitably replaced. Main courses will be a selection of gravy and dry dishes. Keep in mind, 1 veg item is normally a dal.</p>'),(5,'Deserts','<p>Cold deserts normally include the ice cream of the House brand. Cut fruits are another popular cold option. Hot deserts include popular Indian deserts like Gulab Jamun or Kheer.</p>');
/*!40000 ALTER TABLE `venueregistration_mealcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_mealcategoryitem`
--

DROP TABLE IF EXISTS `venueregistration_mealcategoryitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_mealcategoryitem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meal_category_id` int(11) NOT NULL,
  `meal_item_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `venueregistration_mealcategoryitem_2a9db707` (`meal_category_id`),
  KEY `venueregistration_mealcategoryitem_8713e65b` (`meal_item_id`),
  CONSTRAINT `meal_category_id_refs_id_0e1c334e` FOREIGN KEY (`meal_category_id`) REFERENCES `venueregistration_mealcategory` (`id`),
  CONSTRAINT `meal_item_id_refs_id_713453e5` FOREIGN KEY (`meal_item_id`) REFERENCES `venueregistration_mealitem` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_mealcategoryitem`
--

LOCK TABLES `venueregistration_mealcategoryitem` WRITE;
/*!40000 ALTER TABLE `venueregistration_mealcategoryitem` DISABLE KEYS */;
INSERT INTO `venueregistration_mealcategoryitem` VALUES (1,1,1),(2,1,2),(3,1,3),(4,1,4),(5,2,1),(6,2,2),(7,2,3),(8,3,1),(9,3,2),(10,3,3),(11,4,1),(12,4,2),(13,4,3),(14,5,6),(15,5,7);
/*!40000 ALTER TABLE `venueregistration_mealcategoryitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_mealcategoryitemoption`
--

DROP TABLE IF EXISTS `venueregistration_mealcategoryitemoption`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_mealcategoryitemoption` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meal_category_item_id` int(11) NOT NULL,
  `meal_option_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `venueregistration_mealcategoryitemoption_0c8f510b` (`meal_category_item_id`),
  KEY `venueregistration_mealcategoryitemoption_63f0708e` (`meal_option_id`),
  CONSTRAINT `meal_category_item_id_refs_id_c3846324` FOREIGN KEY (`meal_category_item_id`) REFERENCES `venueregistration_mealcategoryitem` (`id`),
  CONSTRAINT `meal_option_id_refs_id_d767fb4b` FOREIGN KEY (`meal_option_id`) REFERENCES `venueregistration_mealoption` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_mealcategoryitemoption`
--

LOCK TABLES `venueregistration_mealcategoryitemoption` WRITE;
/*!40000 ALTER TABLE `venueregistration_mealcategoryitemoption` DISABLE KEYS */;
INSERT INTO `venueregistration_mealcategoryitemoption` VALUES (1,1,1),(2,1,2),(3,1,3),(4,1,4),(5,1,5),(6,2,1),(7,2,2),(8,2,3),(9,2,4),(10,2,5),(11,3,6),(12,3,7),(13,4,8),(14,4,9),(15,4,10),(16,4,11),(17,4,12),(18,5,1),(19,5,2),(20,5,3),(21,6,1),(22,6,2),(23,6,3),(24,7,6),(25,7,7),(26,8,1),(27,8,2),(28,8,3),(29,9,1),(30,9,2),(31,9,3),(32,10,6),(33,10,7),(34,11,1),(35,11,2),(36,11,3),(37,11,4),(38,11,5),(39,12,1),(40,12,2),(41,12,3),(42,12,4),(43,12,5),(44,13,6),(45,13,7),(46,14,1),(47,14,2),(48,14,3),(49,14,4),(50,14,5),(51,15,1),(52,15,2),(53,15,3),(54,15,4),(55,15,5);
/*!40000 ALTER TABLE `venueregistration_mealcategoryitemoption` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_mealitem`
--

DROP TABLE IF EXISTS `venueregistration_mealitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_mealitem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_mealitem`
--

LOCK TABLES `venueregistration_mealitem` WRITE;
/*!40000 ALTER TABLE `venueregistration_mealitem` DISABLE KEYS */;
INSERT INTO `venueregistration_mealitem` VALUES (1,'Veg'),(2,'Non-Veg'),(3,'Seafood in Non-veg'),(4,'Timing'),(5,'IMFL'),(6,'Cold'),(7,'Hot');
/*!40000 ALTER TABLE `venueregistration_mealitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_mealoption`
--

DROP TABLE IF EXISTS `venueregistration_mealoption`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_mealoption` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_mealoption`
--

LOCK TABLES `venueregistration_mealoption` WRITE;
/*!40000 ALTER TABLE `venueregistration_mealoption` DISABLE KEYS */;
INSERT INTO `venueregistration_mealoption` VALUES (1,'0'),(2,'1'),(3,'2'),(4,'3'),(5,'4'),(6,'Yes'),(7,'No'),(8,'60'),(9,'90'),(10,'120'),(11,'180'),(12,'240');
/*!40000 ALTER TABLE `venueregistration_mealoption` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_mealtype`
--

DROP TABLE IF EXISTS `venueregistration_mealtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_mealtype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_mealtype`
--

LOCK TABLES `venueregistration_mealtype` WRITE;
/*!40000 ALTER TABLE `venueregistration_mealtype` DISABLE KEYS */;
INSERT INTO `venueregistration_mealtype` VALUES (1,'Lunch'),(2,'Dinner');
/*!40000 ALTER TABLE `venueregistration_mealtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_otp`
--

DROP TABLE IF EXISTS `venueregistration_otp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_otp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` int(11) NOT NULL,
  `expiry_date` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `venueregistration_otp_6340c63c` (`user_id`),
  CONSTRAINT `user_id_refs_id_7e4479b7` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_otp`
--

LOCK TABLES `venueregistration_otp` WRITE;
/*!40000 ALTER TABLE `venueregistration_otp` DISABLE KEYS */;
/*!40000 ALTER TABLE `venueregistration_otp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_package`
--

DROP TABLE IF EXISTS `venueregistration_package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `custom` tinyint(1) NOT NULL,
  `meal_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `venueregistration_package_8fd47e65` (`meal_type_id`),
  CONSTRAINT `meal_type_id_refs_id_be2161fb` FOREIGN KEY (`meal_type_id`) REFERENCES `venueregistration_mealtype` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_package`
--

LOCK TABLES `venueregistration_package` WRITE;
/*!40000 ALTER TABLE `venueregistration_package` DISABLE KEYS */;
INSERT INTO `venueregistration_package` VALUES (1,'CONFERENCE PACKAGE',0,1),(2,'COCKTAIL PARTY',0,2),(3,'STANDARD MEAL PACKAGE',0,2),(4,'PREMIUM MEAL PACKAGE',0,1),(5,'SNACKS',0,2),(6,'None 12:07PM on August 22, 2013',1,2),(7,'prasanna@beehyv.com 12:17PM on August 22, 2013',1,2),(8,'prasanna@beehyv.com 12:18PM on August 22, 2013',1,2),(9,'prasanna@beehyv.com 12:29PM on August 22, 2013',1,2);
/*!40000 ALTER TABLE `venueregistration_package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_packagecontents`
--

DROP TABLE IF EXISTS `venueregistration_packagecontents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_packagecontents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) DEFAULT NULL,
  `contents_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `venueregistration_packagecontents_b6411b91` (`package_id`),
  KEY `venueregistration_packagecontents_2c8ef674` (`contents_id`),
  CONSTRAINT `contents_id_refs_id_9a2c78ee` FOREIGN KEY (`contents_id`) REFERENCES `venueregistration_mealcategoryitemoption` (`id`),
  CONSTRAINT `package_id_refs_id_2654ea74` FOREIGN KEY (`package_id`) REFERENCES `venueregistration_package` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_packagecontents`
--

LOCK TABLES `venueregistration_packagecontents` WRITE;
/*!40000 ALTER TABLE `venueregistration_packagecontents` DISABLE KEYS */;
INSERT INTO `venueregistration_packagecontents` VALUES (1,6,35),(2,6,41),(3,6,47),(4,7,2),(5,7,7),(6,7,11),(7,7,13),(8,8,2),(9,8,7),(10,8,11),(11,8,13),(12,9,13),(13,9,25);
/*!40000 ALTER TABLE `venueregistration_packagecontents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_packagedescription`
--

DROP TABLE IF EXISTS `venueregistration_packagedescription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_packagedescription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `description` longtext NOT NULL,
  `display_order` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `venueregistration_packagedescription_b6411b91` (`package_id`),
  CONSTRAINT `package_id_refs_id_2cdefd40` FOREIGN KEY (`package_id`) REFERENCES `venueregistration_package` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_packagedescription`
--

LOCK TABLES `venueregistration_packagedescription` WRITE;
/*!40000 ALTER TABLE `venueregistration_packagedescription` DISABLE KEYS */;
INSERT INTO `venueregistration_packagedescription` VALUES (1,1,'Tea/Coffee with Cookies twice a day',1),(2,1,'Standard Meal Package for Lunch',2),(3,2,'Appetizers for 90 minutes',1),(4,2,'IMFL liqour for 90 minutes',2),(5,3,'Appetizers 1 veg and 1 non-veg',1),(6,3,'1 non-veg main course and 3 non-veg main course',2),(7,3,'Rice/Rotis Condiments',3),(8,3,'1 hot and 1 cold desert',4),(9,4,'Appetizers: 2 veg and 2 non-veg',1),(10,4,'Choice of 2 soups and 2 salads',2),(11,4,'2 non-veg main course and 2 veg main course',3),(12,4,'Rice/Rotis, Condiments',4),(13,4,'1 hot and 2 cold deserts',5),(14,5,'2 veg and 2 non-veg appetizers for 120 minutes',1);
/*!40000 ALTER TABLE `venueregistration_packagedescription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_quote`
--

DROP TABLE IF EXISTS `venueregistration_quote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_quote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `package_id` int(11) DEFAULT NULL,
  `quote_key` varchar(255) DEFAULT NULL,
  `guests_id` int(11) NOT NULL,
  `venuetype_id` int(11) NOT NULL,
  `occasion_id` int(11) NOT NULL,
  `date_of_booking` datetime NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `venueregistration_quote_6340c63c` (`user_id`),
  KEY `venueregistration_quote_b6411b91` (`package_id`),
  KEY `venueregistration_quote_cba0ea91` (`guests_id`),
  KEY `venueregistration_quote_1b0038fb` (`venuetype_id`),
  KEY `venueregistration_quote_7a282436` (`occasion_id`),
  CONSTRAINT `guests_id_refs_id_418a9c43` FOREIGN KEY (`guests_id`) REFERENCES `venueregistration_capacityrange` (`id`),
  CONSTRAINT `occasion_id_refs_id_2a5ba2bf` FOREIGN KEY (`occasion_id`) REFERENCES `venueregistration_eventtype` (`id`),
  CONSTRAINT `package_id_refs_id_d8885ce7` FOREIGN KEY (`package_id`) REFERENCES `venueregistration_package` (`id`),
  CONSTRAINT `user_id_refs_id_a7d174f4` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `venuetype_id_refs_id_e63ac2d9` FOREIGN KEY (`venuetype_id`) REFERENCES `venueregistration_venuetype` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_quote`
--

LOCK TABLES `venueregistration_quote` WRITE;
/*!40000 ALTER TABLE `venueregistration_quote` DISABLE KEYS */;
/*!40000 ALTER TABLE `venueregistration_quote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_quotevenue`
--

DROP TABLE IF EXISTS `venueregistration_quotevenue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_quotevenue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quote_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `venueregistration_quotevenue_d4162e45` (`quote_id`),
  KEY `venueregistration_quotevenue_03eb0938` (`venue_id`),
  CONSTRAINT `quote_id_refs_id_163f965b` FOREIGN KEY (`quote_id`) REFERENCES `venueregistration_quote` (`id`),
  CONSTRAINT `venue_id_refs_id_e8486845` FOREIGN KEY (`venue_id`) REFERENCES `venueregistration_venue` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_quotevenue`
--

LOCK TABLES `venueregistration_quotevenue` WRITE;
/*!40000 ALTER TABLE `venueregistration_quotevenue` DISABLE KEYS */;
/*!40000 ALTER TABLE `venueregistration_quotevenue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_seatingoption`
--

DROP TABLE IF EXISTS `venueregistration_seatingoption`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_seatingoption` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_seatingoption`
--

LOCK TABLES `venueregistration_seatingoption` WRITE;
/*!40000 ALTER TABLE `venueregistration_seatingoption` DISABLE KEYS */;
INSERT INTO `venueregistration_seatingoption` VALUES (1,'Swivel Chairs'),(2,'Plastic Chairs');
/*!40000 ALTER TABLE `venueregistration_seatingoption` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_state`
--

DROP TABLE IF EXISTS `venueregistration_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `venueregistration_state_d860be3c` (`country_id`),
  CONSTRAINT `country_id_refs_id_ba40c3f3` FOREIGN KEY (`country_id`) REFERENCES `venueregistration_country` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_state`
--

LOCK TABLES `venueregistration_state` WRITE;
/*!40000 ALTER TABLE `venueregistration_state` DISABLE KEYS */;
INSERT INTO `venueregistration_state` VALUES (1,'Andhra Pradesh',1),(2,'Karnataka',1);
/*!40000 ALTER TABLE `venueregistration_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_userprofile`
--

DROP TABLE IF EXISTS `venueregistration_userprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_userprofile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `company` varchar(128) DEFAULT NULL,
  `mobile_number` bigint(20) NOT NULL,
  `is_verified` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `user_id_refs_id_3504fec6` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_userprofile`
--

LOCK TABLES `venueregistration_userprofile` WRITE;
/*!40000 ALTER TABLE `venueregistration_userprofile` DISABLE KEYS */;
INSERT INTO `venueregistration_userprofile` VALUES (1,1,'Beehyv Software Solutions',9676589044,0);
/*!40000 ALTER TABLE `venueregistration_userprofile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_venue`
--

DROP TABLE IF EXISTS `venueregistration_venue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_venue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `manager_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `description` varchar(128) DEFAULT NULL,
  `venue_type_id` int(11) NOT NULL,
  `capacity` int(11) NOT NULL,
  `minimum_occupancy` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `last_modified_at` date NOT NULL,
  `rating` decimal(2,1) NOT NULL,
  `is_pure_veg` tinyint(1) NOT NULL,
  `serves_liquor` tinyint(1) NOT NULL,
  `base_package_rate` decimal(10,2) NOT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `address3` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `venueregistration_venue_68afd4a7` (`manager_id`),
  KEY `venueregistration_venue_afbb987d` (`location_id`),
  KEY `venueregistration_venue_e78989b2` (`venue_type_id`),
  CONSTRAINT `location_id_refs_id_da61a50c` FOREIGN KEY (`location_id`) REFERENCES `venueregistration_location` (`id`),
  CONSTRAINT `manager_id_refs_id_8d62360b` FOREIGN KEY (`manager_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `venue_type_id_refs_id_86212241` FOREIGN KEY (`venue_type_id`) REFERENCES `venueregistration_venuetype` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_venue`
--

LOCK TABLES `venueregistration_venue` WRITE;
/*!40000 ALTER TABLE `venueregistration_venue` DISABLE KEYS */;
INSERT INTO `venueregistration_venue` VALUES (7,'The Purple Leaf Hotel',1,1,'The Purple Leaf Hotel',1,150,0,'2013-08-30','2013-08-30',3.0,0,0,10000.00,'Kharkhana Road','Trimulgherry','Secunderabad'),(8,'Hotel1',1,1,'The Purple Leaf Hotel',1,150,0,'2013-08-30','2013-08-30',3.0,0,0,10000.00,'Kharkhana Road','Trimulgherry','Secunderabad'),(9,'Hotel2',1,1,'The Purple Leaf Hotel',1,150,0,'2013-08-30','2013-08-30',3.0,0,0,10000.00,'Kharkhana Road','Trimulgherry','Secunderabad'),(10,'Hotel3',1,1,'The Purple Leaf Hotel',1,150,0,'2013-08-30','2013-08-30',3.0,0,0,10000.00,'Kharkhana Road','Trimulgherry','Secunderabad'),(11,'Hotel4',1,1,'The Purple Leaf Hotel',1,150,0,'2013-08-30','2013-08-30',3.0,0,0,10000.00,'Kharkhana Road','Trimulgherry','Secunderabad'),(12,'Hotel5',1,1,'The Purple Leaf Hotel',1,150,0,'2013-08-30','2013-08-30',3.0,0,0,10000.00,'Kharkhana Road','Trimulgherry','Secunderabad'),(13,'Hotel6',1,1,'The Purple Leaf Hotel',1,150,0,'2013-08-30','2013-08-30',3.0,0,0,10000.00,'Kharkhana Road','Trimulgherry','Secunderabad'),(14,'Hotel7',1,1,'The Purple Leaf Hotel',1,150,0,'2013-08-30','2013-08-30',3.0,0,0,25000.00,'Kharkhana Road','Trimulgherry','Secunderabad'),(15,'Hotel8',1,1,'The Purple Leaf Hotel',1,150,0,'2013-08-30','2013-08-30',3.0,0,0,10000.00,'Kharkhana Road','Trimulgherry','Secunderabad'),(16,'Hotel9',1,2,'Test Hotel',1,150,0,'2013-08-30','2013-08-30',3.0,0,0,25000.00,'Road No.1','Banjara Hills','Hyderabad'),(17,'Hotel10',1,2,'Hotel10',1,15,0,'2013-08-30','2013-08-30',3.0,0,0,18000.00,'Road No.1','Banjara Hills','Hyderabad'),(18,'Hotel11',1,2,'Hotel11',1,150,0,'2013-08-30','2013-08-30',3.0,0,0,21000.00,'Road No.1','Banjara Hills','Hyderabad');
/*!40000 ALTER TABLE `venueregistration_venue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_venue_event_type`
--

DROP TABLE IF EXISTS `venueregistration_venue_event_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_venue_event_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `venue_id` int(11) NOT NULL,
  `eventtype_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `venueregistration_venue_event_type_venue_id_1656c7fd_uniq` (`venue_id`,`eventtype_id`),
  KEY `venueregistration_venue_event_type_03eb0938` (`venue_id`),
  KEY `venueregistration_venue_event_type_7a4f8c6c` (`eventtype_id`),
  CONSTRAINT `eventtype_id_refs_id_0f7fe508` FOREIGN KEY (`eventtype_id`) REFERENCES `venueregistration_eventtype` (`id`),
  CONSTRAINT `venue_id_refs_id_b3789c43` FOREIGN KEY (`venue_id`) REFERENCES `venueregistration_venue` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_venue_event_type`
--

LOCK TABLES `venueregistration_venue_event_type` WRITE;
/*!40000 ALTER TABLE `venueregistration_venue_event_type` DISABLE KEYS */;
INSERT INTO `venueregistration_venue_event_type` VALUES (37,7,4),(38,7,5),(39,7,6),(40,7,7),(41,7,8),(42,7,9),(43,7,10),(44,7,11),(45,8,4),(46,8,5),(47,8,6),(48,8,7),(49,8,8),(50,8,9),(51,8,10),(52,8,11),(53,9,4),(54,9,5),(55,9,6),(56,9,7),(57,9,8),(58,9,9),(59,9,10),(60,9,11),(61,10,4),(62,10,5),(63,10,6),(64,10,7),(65,10,8),(66,10,9),(67,10,10),(68,10,11),(69,11,4),(70,11,5),(71,11,6),(72,11,7),(73,11,8),(74,11,9),(75,11,10),(76,11,11),(77,12,4),(78,12,5),(79,12,6),(80,12,7),(81,12,8),(82,12,9),(83,12,10),(84,12,11),(85,13,4),(86,13,5),(87,13,6),(88,13,7),(89,13,8),(90,13,9),(91,13,10),(92,13,11),(93,14,4),(94,14,5),(95,14,6),(96,14,7),(97,14,8),(98,14,9),(99,14,10),(100,14,11),(101,15,4),(102,15,5),(103,15,6),(104,15,7),(105,15,8),(106,15,9),(107,15,10),(108,15,11),(109,16,4),(110,16,5),(111,16,6),(112,16,7),(113,16,8),(114,16,9),(115,16,10),(116,16,11),(125,17,4),(126,17,5),(127,17,6),(128,17,7),(129,17,8),(130,17,9),(131,17,10),(132,17,11),(133,18,4),(134,18,5),(135,18,6),(136,18,7),(137,18,8),(138,18,9),(139,18,10),(140,18,11);
/*!40000 ALTER TABLE `venueregistration_venue_event_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_venuebookinghistory`
--

DROP TABLE IF EXISTS `venueregistration_venuebookinghistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_venuebookinghistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `venue_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  PRIMARY KEY (`id`),
  KEY `venueregistration_venuebookinghistory_03eb0938` (`venue_id`),
  CONSTRAINT `venue_id_refs_id_da5937c6` FOREIGN KEY (`venue_id`) REFERENCES `venueregistration_venue` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_venuebookinghistory`
--

LOCK TABLES `venueregistration_venuebookinghistory` WRITE;
/*!40000 ALTER TABLE `venueregistration_venuebookinghistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `venueregistration_venuebookinghistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_venuepicture`
--

DROP TABLE IF EXISTS `venueregistration_venuepicture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_venuepicture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `venue_id` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `displayable` tinyint(1) NOT NULL,
  `is_display_pic` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `venueregistration_venuepicture_03eb0938` (`venue_id`),
  CONSTRAINT `venue_id_refs_id_eed05020` FOREIGN KEY (`venue_id`) REFERENCES `venueregistration_venue` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_venuepicture`
--

LOCK TABLES `venueregistration_venuepicture` WRITE;
/*!40000 ALTER TABLE `venueregistration_venuepicture` DISABLE KEYS */;
INSERT INTO `venueregistration_venuepicture` VALUES (9,7,'venue_images/Chrysanthemum_5.jpg',1,1),(10,7,'venue_images/Desert.jpg',1,0),(11,8,'venue_images/Penguins.jpg',1,1),(12,8,'venue_images/Tulips.jpg',1,0),(13,9,'venue_images/Koala.jpg',1,1),(14,9,'venue_images/Lighthouse.jpg',1,0),(15,10,'venue_images/Desert_1.jpg',1,1),(16,10,'venue_images/Jellyfish.jpg',1,0),(17,11,'venue_images/Chrysanthemum_6.jpg',1,1),(18,11,'venue_images/Koala_1.jpg',1,0),(19,12,'venue_images/Desert_2.jpg',1,1),(20,12,'venue_images/Jellyfish_1.jpg',1,0),(21,12,'venue_images/Lighthouse_1.jpg',1,0),(22,13,'venue_images/Koala_2.jpg',1,1),(23,13,'venue_images/Penguins_1.jpg',1,0),(24,14,'venue_images/Penguins_2.jpg',1,1),(25,14,'venue_images/Koala_3.jpg',1,0),(26,15,'venue_images/Penguins_3.jpg',1,1),(27,15,'venue_images/Chrysanthemum_7.jpg',1,0),(28,16,'venue_images/Koala_4.jpg',1,1),(29,16,'venue_images/Desert_3.jpg',1,0),(30,17,'venue_images/Desert_4.jpg',1,1),(31,17,'venue_images/Penguins_4.jpg',1,0),(32,18,'venue_images/Tulips_1.jpg',1,1),(33,18,'venue_images/Lighthouse_2.jpg',1,0);
/*!40000 ALTER TABLE `venueregistration_venuepicture` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_venuerating`
--

DROP TABLE IF EXISTS `venueregistration_venuerating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_venuerating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `venue_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating` decimal(2,1) NOT NULL,
  `review` longtext,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `venueregistration_venuerating_03eb0938` (`venue_id`),
  KEY `venueregistration_venuerating_6340c63c` (`user_id`),
  CONSTRAINT `user_id_refs_id_38f5957d` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `venue_id_refs_id_934477df` FOREIGN KEY (`venue_id`) REFERENCES `venueregistration_venue` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_venuerating`
--

LOCK TABLES `venueregistration_venuerating` WRITE;
/*!40000 ALTER TABLE `venueregistration_venuerating` DISABLE KEYS */;
/*!40000 ALTER TABLE `venueregistration_venuerating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_venuesupportseventtype`
--

DROP TABLE IF EXISTS `venueregistration_venuesupportseventtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_venuesupportseventtype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `venue_id` int(11) NOT NULL,
  `event_type_id` int(11) NOT NULL,
  `evnet_subtype_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `venueregistration_venuesupportseventtype_03eb0938` (`venue_id`),
  KEY `venueregistration_venuesupportseventtype_3120f0b2` (`event_type_id`),
  CONSTRAINT `event_type_id_refs_id_e6cc97f7` FOREIGN KEY (`event_type_id`) REFERENCES `venueregistration_eventtype` (`id`),
  CONSTRAINT `venue_id_refs_id_a2f34a24` FOREIGN KEY (`venue_id`) REFERENCES `venueregistration_venue` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_venuesupportseventtype`
--

LOCK TABLES `venueregistration_venuesupportseventtype` WRITE;
/*!40000 ALTER TABLE `venueregistration_venuesupportseventtype` DISABLE KEYS */;
/*!40000 ALTER TABLE `venueregistration_venuesupportseventtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venueregistration_venuetype`
--

DROP TABLE IF EXISTS `venueregistration_venuetype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venueregistration_venuetype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venueregistration_venuetype`
--

LOCK TABLES `venueregistration_venuetype` WRITE;
/*!40000 ALTER TABLE `venueregistration_venuetype` DISABLE KEYS */;
INSERT INTO `venueregistration_venuetype` VALUES (1,'Hotel');
/*!40000 ALTER TABLE `venueregistration_venuetype` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-08-30 16:52:40
