from django.conf.urls import patterns, include, url
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from django.conf.urls.static import static
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
# from venueregistration.admin import myadmin
from venueregistration import views,urls as venueurls
from venueregistration.admin import user_admin_site
admin.autodiscover()


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'superplanner.views.home', name='home'),
    # url(r'^superplanner/', include('superplanner.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    #url(r'^$', include('venueregistration.urls')),
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^auth/$', include('social_auth.urls'), name='auth'),
    
    url(r'', include('social_auth.urls')),
    # Uncomment the next line to enable the admin:
#    url(r'^login/', AuthenticationForm)
    url(r'^admin/', include(admin.site.urls)),
    url(r'^venueadmin/', include(user_admin_site.urls)),
    url(r'', include(venueurls)),
    url(r'^login/$', 'django.contrib.auth.views.login')
    
)

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
