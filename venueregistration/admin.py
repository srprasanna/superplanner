from django.contrib import admin
from venueregistration.models import (
    EventType, EventSubType, Country, State, City, Location, VenueType,
    SeatingOption, MealType, CapacityRange, MealCategory,MealItem,MealCategoryItem,Venue,VenuePicture,VenueRating,VenueBookingHistory,QuoteVenue,Quote,Package,RelatedVenues,MealOption,MealCategoryItemOption,PackageDescription,BookingTimeType
)
from venueregistration.forms import UserAdminAuthenticationForm

class EventSubTypeInline(admin.StackedInline):    
    model = EventSubType
    extra = 3
    
class EventTypeAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['name']})
    ]
    inlines = [EventSubTypeInline]

admin.site.register(EventType, EventTypeAdmin)

class StateInline(admin.StackedInline):    
    model = State
    extra = 3
    
class CountryAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['name']})
    ]
    inlines = [StateInline]

admin.site.register(Country, CountryAdmin)

class LocationInline(admin.StackedInline):    
    model = Location
    extra = 3
    
class CityAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ('name', 'state')})
    ]
    inlines = [LocationInline]


class PackageDescriptionInline(admin.StackedInline):
    model=PackageDescription


class PackageAdmin(admin.ModelAdmin):
    inlines=[PackageDescriptionInline]
    def queryset(self, request):
        qs = admin.ModelAdmin.queryset(self, request)
        return qs.filter(custom=False)


class VenuePictureInline(admin.StackedInline):
    model=VenuePicture




class VenueAdmin(admin.ModelAdmin):
    inlines=[VenuePictureInline]


class RelatedVenueAdmin(admin.ModelAdmin):
    pass



admin.site.register(Package,PackageAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(Venue,VenueAdmin)
admin.site.register(VenueType)
admin.site.register(SeatingOption)
admin.site.register(CapacityRange)
admin.site.register(MealType)
admin.site.register(MealCategory)
admin.site.register(MealItem)
admin.site.register(MealCategoryItem)
admin.site.register(MealOption)
admin.site.register(MealCategoryItemOption)
admin.site.register(RelatedVenues)
admin.site.register(BookingTimeType)


from django.contrib.admin.sites import AdminSite
class UserAdmin(AdminSite):
    login_form = UserAdminAuthenticationForm
    def has_permission(self, request):
        """
        Removed check for is_staff.
        """
        return request.user.is_active


user_admin_site = UserAdmin(name='usersadmin')

class FilterUserAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()

    def queryset(self, request):
        qs = admin.ModelAdmin.queryset(self, request)
        try:
            qs.filter(manager=request.user)
            return qs
        except:
            venue = Venue.objects.filter(manager=request.user)
            qs.filter(venue=venue)
            return qs


    # def has_change_permission(self, request, obj=None):
    #     if not obj:
    #         # the changelist itself
    #         return True
    #     return obj.user == request.user




class VenuePictureInline(admin.StackedInline):
    model=VenuePicture


class VenueBookingHistoryInline(admin.StackedInline):
    model=VenueBookingHistory

class QuotesInline(admin.StackedInline):
    model=QuoteVenue

class VenueAdmin(FilterUserAdmin):
    inlines = [VenuePictureInline,VenueBookingHistoryInline,QuotesInline]

    def save_model(self, request, obj, form, change):
        obj.manager = request.user
        obj.save()

    def get_form(self, request, obj=None, **kwargs):
        self.exclude = []
        self.exclude.append('manager')
        return super(VenueAdmin, self).get_form(request, obj, **kwargs)

    pass

# Run user_admin_site.register() for each model we wish to register
# for our admin interface for users

# Run admin.site.register() for each model we wish to register
# with the REAL django admin!



class VenueRatingAdmin(FilterUserAdmin):
    list_display=("review","rating","venue","user")
    pass


user_admin_site.register(Venue,VenueAdmin)
#user_admin_site.register(VenuePicture,VenuePictureAdmin)
user_admin_site.register(VenueRating,VenueRatingAdmin)