from __future__ import unicode_literals
import user
from django.http import HttpResponse
from django.utils.formats import get_format
from django.views.generic.edit import FormView
from django.views.generic import View, TemplateView


from django.utils.encoding import force_text
from django.utils.functional import lazy
from django.utils import six
from django.utils import translation
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.shortcuts import redirect, render  # , render_to_response
from django.core.mail import send_mail
import time
from setuptools import package_index
from sms import send_sms_msg
from util import id_generator
from django.utils.dateformat import DateFormat, TimeFormat
from django.utils.formats import get_format
# from django.contrib.formtools.wizard.views import SessionWizardView

import re

import simplejson, datetime,json,datetime
from django import forms

from venueregistration.models import City, SeatingOption, CapacityRange, VenueType, EventType,BookingTimeType

class ContactForm(forms.Form):
    name=forms.CharField(required=True)
    company=forms.CharField(required=False)
    email=forms.EmailField(required=True)
    mobile=forms.IntegerField(required=False,min_value=1000000000,max_value=9999999999)

class SearchForm(forms.Form):
    
    def __init__(self, *args, **kwargs):
        super(SearchForm, self).__init__(*args, **kwargs)
        list_of_cities = City.objects.all()
        city_dict = ((city.id, city.name) for city in list_of_cities)        
        self.fields['city'].choices = city_dict
        
        list_of_seating_types = SeatingOption.objects.all()
        seating_dict = ((seating.id, seating.name) for seating in list_of_seating_types)        
        self.fields['seating'].choices = seating_dict
        
        list_of_events = EventType.objects.all()
        event_type_dict = ((event_type.id, event_type.name) for event_type in list_of_events)
        self.fields['occasion'].choices = event_type_dict
        self.fields['occasion'].widget.attrs.update({'style' : 'width:300px;'})
        
        list_of_capacities = CapacityRange.objects.all()
        capacity_dict = ((capacity.id, capacity.display_string) for capacity in list_of_capacities)
        self.fields['guests'].choices = capacity_dict
        
        list_of_venue_types = VenueType.objects.all()
        venue_type_dict = ((venue_type.id, venue_type.name) for venue_type in list_of_venue_types)
        self.fields['environment'].choices = venue_type_dict
        
        list_of_time_slot = BookingTimeType.objects.all()
        time_slot_dict = ((time_slot.id,time_slot.name) for time_slot in list_of_time_slot)
        self.fields['time'].choices = time_slot_dict
        
    city = forms.ChoiceField()
    locality = forms.MultipleChoiceField(required=False)    
    occasion = forms.MultipleChoiceField(required=False)
    date = forms.DateField(input_formats="%d/%m/%Y")
    time = forms.ChoiceField(required=True)
    guests = forms.ChoiceField()
    seating = forms.ChoiceField(required=True)
    environment = forms.MultipleChoiceField(required=False)
    
class SearchResultsForm(forms.Form):
    
    def __init__(self, *args, **kwargs):
        super(SearchResultsForm, self).__init__(*args, **kwargs)
        list_of_cities = City.objects.all()
        city_dict = ((city.id, city.name) for city in list_of_cities)        
        self.fields['city'].choices = city_dict
        
        list_of_seating_types = SeatingOption.objects.all()
        seating_dict = ((seating.id, seating.name) for seating in list_of_seating_types)        
        self.fields['seating'].choices = seating_dict
        
        list_of_capacities = CapacityRange.objects.all()
        capacity_dict = ((capacity.id, capacity.display_string) for capacity in list_of_capacities)
        self.fields['guests'].choices = capacity_dict
        
        list_of_venue_types = VenueType.objects.all()
        venue_type_dict = ((venue_type.id, venue_type.name) for venue_type in list_of_venue_types)
        self.fields['environment'].choices = venue_type_dict
        
        list_of_time_slot = BookingTimeType.objects.all()
        time_slot_dict = ((time_slot.id,time_slot.name) for time_slot in list_of_time_slot)
        self.fields['time'].choices = time_slot_dict
        
        list_of_events = EventType.objects.all()
        event_type_dict = ((event_type.id, event_type.name) for event_type in list_of_events)
        self.fields['occasion'].choices = event_type_dict
        self.fields['occasion'].widget.attrs.update({'style' : 'width:300px;'})
        
        self.fields['sort_by'].choices = {'rating': 'rating'}
        
    city = forms.ChoiceField()
    locality = forms.MultipleChoiceField(required=False)    
    occasion = forms.MultipleChoiceField(required=False)
    date = forms.DateField(input_formats="%d/%m/%Y")
    time = forms.ChoiceField(required=True)
    guests = forms.ChoiceField()
    seating = forms.ChoiceField(required=True)
    environment = forms.MultipleChoiceField(required=False)
    sort_by = forms.ChoiceField()
    
class ShortlistForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ShortlistForm, self).__init__(*args, **kwargs)

    venues = forms.MultipleChoiceField()

class GetQuoteForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(GetQuoteForm, self).__init__(*args, **kwargs)
    f_and_b = forms.BooleanField()
    name = forms.CharField()
    address = forms.CharField()
    company = forms.CharField()
    email = forms.EmailField()
    mobile = forms.IntegerField()

class FoodAndBeveragesForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(FoodAndBeveragesForm, self).__init__(*args, **kwargs)

    selection_info = forms.CharField()



from django.contrib.auth.forms import AuthenticationForm

class UserAdminAuthenticationForm(AuthenticationForm):
    """
    Same as Django's AdminAuthenticationForm but allows to login
    any user who is not staff.
    """
    this_is_the_login_form = forms.BooleanField(widget=forms.HiddenInput,
                                                initial=1,
                                                error_messages={'required': "Please log in again, because your session has expired."})

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        #message = ERROR_MESSAGE

        if username and password:
            self.user_cache = authenticate(username=username,
                                           password=password)
            if self.user_cache is None:
                if u'@' in username:
                    # Mistakenly entered e-mail address instead of username?
                    # Look it up.
                    try:
                        user = User.objects.get(email=username)
                    except (User.DoesNotExist, User.MultipleObjectsReturned):
                        # Nothing to do here, moving along.
                        pass
                    else:
                        if user.check_password(password):
                            message =u"Your e-mail address is not your username. Try '%s' instead." % user.username
                raise forms.ValidationError(message)
            # Removed check for is_staff here!
            elif not self.user_cache.is_active:
                raise forms.ValidationError(message)
        self.check_for_test_cookie()
        return self.cleaned_data
