import requests

def get_location(ip):
    url = "http://api.ipaddresslabs.com/iplocation/v1.7/locateip"
    payload = {'format' : 'json',
              'ip' : ip,
              'key' : "demo"
    }
    r=requests.get(url,params=payload)
    text = r.json()
    try:
        return text["geolocation_data"]["city"]
    except:
        return "Hyderabad"




def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip    