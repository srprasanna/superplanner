from django.core.management.base import BaseCommand, CommandError
from venueregistration.models import VenueRating, Venue


class Command(BaseCommand):
    args = '<venue_id venue_id ...>'
    help = 'Calculates Average Ratings for venues'

    def compute_ratings(self):
        try:
            ratings = []
            max_length_review = 12
            venue_name = ""
            review_length = len(self.venue_ratings)
            if review_length>=max_length_review:
                for venue_rating in self.venue_ratings:
                    if venue_rating.rating > 0:
                        ratings.append(venue_rating.rating)
            else:
                ratings.append(3)

            venue = self.venue
            average_ratings = round(sum(ratings)/len(ratings),1)
            venue.rating = average_ratings
            venue_name = venue.name
            venue.review_count = review_length
            venue.save()
            self.stdout.write('Ratings for venue %s is %s' % (venue_name,str(average_ratings)))
        except Exception, e:
            self.stdout.write("Unable to calculate ratings owing to %s" % str(e))


    def handle(self, *args, **options):
        args_length = len(args)
        if args_length > 0:
            for venue_id in args:
                venue = Venue.objects.filter(pk=int(venue_id))
                self.venue = venue
                venue_ratings = VenueRating.objects.filter(venue=venue)
                self.venue_ratings = venue_ratings
                self.compute_ratings()
        else:
            venues = Venue.objects.all()
            for venue in venues:
                self.venue = venue
                venue_ratings = VenueRating.objects.filter(venue=venue)
                self.venue_ratings=venue_ratings
                self.compute_ratings()

                #self.stdout.write('Length of arguments  "%s"' % length)