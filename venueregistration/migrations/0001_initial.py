# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'UserProfile'
        db.create_table(u'venueregistration_userprofile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True)),
            ('company', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('mobile_number', self.gf('django.db.models.fields.BigIntegerField')()),
        ))
        db.send_create_signal(u'venueregistration', ['UserProfile'])

        # Adding model 'OTP'
        db.create_table(u'venueregistration_otp', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('password', self.gf('django.db.models.fields.IntegerField')()),
            ('expiry_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
        ))
        db.send_create_signal(u'venueregistration', ['OTP'])

        # Adding model 'EventType'
        db.create_table(u'venueregistration_eventtype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
        ))
        db.send_create_signal(u'venueregistration', ['EventType'])

        # Adding model 'EventSubType'
        db.create_table(u'venueregistration_eventsubtype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('event_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.EventType'])),
        ))
        db.send_create_signal(u'venueregistration', ['EventSubType'])

        # Adding model 'Country'
        db.create_table(u'venueregistration_country', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
        ))
        db.send_create_signal(u'venueregistration', ['Country'])

        # Adding model 'State'
        db.create_table(u'venueregistration_state', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.Country'])),
        ))
        db.send_create_signal(u'venueregistration', ['State'])

        # Adding model 'City'
        db.create_table(u'venueregistration_city', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('state', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.State'])),
        ))
        db.send_create_signal(u'venueregistration', ['City'])

        # Adding model 'Location'
        db.create_table(u'venueregistration_location', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('city', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.City'])),
        ))
        db.send_create_signal(u'venueregistration', ['Location'])

        # Adding model 'SeatingOption'
        db.create_table(u'venueregistration_seatingoption', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
        ))
        db.send_create_signal(u'venueregistration', ['SeatingOption'])

        # Adding model 'VenueType'
        db.create_table(u'venueregistration_venuetype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
        ))
        db.send_create_signal(u'venueregistration', ['VenueType'])

        # Adding model 'CapacityRange'
        db.create_table(u'venueregistration_capacityrange', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('minimum', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('maximum', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('display_string', self.gf('django.db.models.fields.CharField')(max_length=128)),
        ))
        db.send_create_signal(u'venueregistration', ['CapacityRange'])

        # Adding model 'MealType'
        db.create_table(u'venueregistration_mealtype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
        ))
        db.send_create_signal(u'venueregistration', ['MealType'])

        # Adding model 'MealItemCategory'
        db.create_table(u'venueregistration_mealitemcategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=1024, null=True, blank=True)),
        ))
        db.send_create_signal(u'venueregistration', ['MealItemCategory'])

        # Adding model 'MealTypePackage'
        db.create_table(u'venueregistration_mealtypepackage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('meal_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.MealType'])),
        ))
        db.send_create_signal(u'venueregistration', ['MealTypePackage'])

        # Adding model 'MealCategoryPackage'
        db.create_table(u'venueregistration_mealcategorypackage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('meal_type_package', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.MealTypePackage'])),
            ('meal_category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.MealItemCategory'])),
            ('number_of_veg_items', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('number_of_non_veg_items', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('sea_food_in_non_veg', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('duration', self.gf('django.db.models.fields.IntegerField')(default=None, null=True, blank=True)),
        ))
        db.send_create_signal(u'venueregistration', ['MealCategoryPackage'])

        # Adding model 'MealPackage'
        db.create_table(u'venueregistration_mealpackage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
        ))
        db.send_create_signal(u'venueregistration', ['MealPackage'])

        # Adding M2M table for field meal_type_package on 'MealPackage'
        m2m_table_name = db.shorten_name(u'venueregistration_mealpackage_meal_type_package')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('mealpackage', models.ForeignKey(orm[u'venueregistration.mealpackage'], null=False)),
            ('mealtypepackage', models.ForeignKey(orm[u'venueregistration.mealtypepackage'], null=False))
        ))
        db.create_unique(m2m_table_name, ['mealpackage_id', 'mealtypepackage_id'])

        # Adding model 'Venue'
        db.create_table(u'venueregistration_venue', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('manager', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('location', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.Location'])),
            ('description', self.gf('django.db.models.fields.CharField')(default=None, max_length=128, null=True, blank=True)),
            ('venue_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.VenueType'])),
            ('capacity', self.gf('django.db.models.fields.IntegerField')()),
            ('minimum_occupancy', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('created_at', self.gf('django.db.models.fields.DateField')()),
            ('last_modified_at', self.gf('django.db.models.fields.DateField')()),
            ('rating', self.gf('django.db.models.fields.DecimalField')(default=3.0, max_digits=2, decimal_places=1)),
            ('is_pure_veg', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('serves_liquor', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'venueregistration', ['Venue'])

        # Adding M2M table for field event_type on 'Venue'
        m2m_table_name = db.shorten_name(u'venueregistration_venue_event_type')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('venue', models.ForeignKey(orm[u'venueregistration.venue'], null=False)),
            ('eventtype', models.ForeignKey(orm[u'venueregistration.eventtype'], null=False))
        ))
        db.create_unique(m2m_table_name, ['venue_id', 'eventtype_id'])

        # Adding model 'VenueRating'
        db.create_table(u'venueregistration_venuerating', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('venue', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.Venue'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('rating', self.gf('django.db.models.fields.DecimalField')(default=3.0, max_digits=2, decimal_places=1)),
        ))
        db.send_create_signal(u'venueregistration', ['VenueRating'])

        # Adding model 'CustomOption'
        db.create_table(u'venueregistration_customoption', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('venue', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.Venue'])),
        ))
        db.send_create_signal(u'venueregistration', ['CustomOption'])

        # Adding model 'VenueSupportsEventType'
        db.create_table(u'venueregistration_venuesupportseventtype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('venue', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.Venue'])),
            ('event_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.EventType'])),
            ('evnet_subtype_id', self.gf('django.db.models.fields.IntegerField')(default=None, null=True, blank=True)),
        ))
        db.send_create_signal(u'venueregistration', ['VenueSupportsEventType'])

        # Adding model 'VenuePicture'
        db.create_table(u'venueregistration_venuepicture', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('venue', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.Venue'])),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('displayable', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_display_pic', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'venueregistration', ['VenuePicture'])

        # Adding model 'VenuePackage'
        db.create_table(u'venueregistration_venuepackage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('venue', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.Venue'])),
            ('price_per_head', self.gf('django.db.models.fields.DecimalField')(max_digits=6, decimal_places=2)),
        ))
        db.send_create_signal(u'venueregistration', ['VenuePackage'])

        # Adding M2M table for field meal_package on 'VenuePackage'
        m2m_table_name = db.shorten_name(u'venueregistration_venuepackage_meal_package')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('venuepackage', models.ForeignKey(orm[u'venueregistration.venuepackage'], null=False)),
            ('mealtypepackage', models.ForeignKey(orm[u'venueregistration.mealtypepackage'], null=False))
        ))
        db.create_unique(m2m_table_name, ['venuepackage_id', 'mealtypepackage_id'])

        # Adding M2M table for field event_type on 'VenuePackage'
        m2m_table_name = db.shorten_name(u'venueregistration_venuepackage_event_type')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('venuepackage', models.ForeignKey(orm[u'venueregistration.venuepackage'], null=False)),
            ('eventtype', models.ForeignKey(orm[u'venueregistration.eventtype'], null=False))
        ))
        db.create_unique(m2m_table_name, ['venuepackage_id', 'eventtype_id'])

        # Adding model 'VenuePricePerItem'
        db.create_table(u'venueregistration_venuepriceperitem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('venue', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.Venue'])),
            ('menu_category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.MealItemCategory'])),
            ('price_per_item', self.gf('django.db.models.fields.DecimalField')(max_digits=6, decimal_places=2)),
        ))
        db.send_create_signal(u'venueregistration', ['VenuePricePerItem'])

        # Adding model 'VenueBookingHistory'
        db.create_table(u'venueregistration_venuebookinghistory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('venue', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.Venue'])),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('start_time', self.gf('django.db.models.fields.TimeField')()),
            ('end_time', self.gf('django.db.models.fields.TimeField')()),
        ))
        db.send_create_signal(u'venueregistration', ['VenueBookingHistory'])

        # Adding model 'Quote'
        db.create_table(u'venueregistration_quote', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('time', self.gf('django.db.models.fields.TimeField')()),
            ('meal_package', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.MealPackage'], null=True, blank=True)),
        ))
        db.send_create_signal(u'venueregistration', ['Quote'])

        # Adding M2M table for field venue on 'Quote'
        m2m_table_name = db.shorten_name(u'venueregistration_quote_venue')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('quote', models.ForeignKey(orm[u'venueregistration.quote'], null=False)),
            ('venue', models.ForeignKey(orm[u'venueregistration.venue'], null=False))
        ))
        db.create_unique(m2m_table_name, ['quote_id', 'venue_id'])

        # Adding M2M table for field meal_selections on 'Quote'
        m2m_table_name = db.shorten_name(u'venueregistration_quote_meal_selections')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('quote', models.ForeignKey(orm[u'venueregistration.quote'], null=False)),
            ('mealtypepackage', models.ForeignKey(orm[u'venueregistration.mealtypepackage'], null=False))
        ))
        db.create_unique(m2m_table_name, ['quote_id', 'mealtypepackage_id'])


    def backwards(self, orm):
        # Deleting model 'UserProfile'
        db.delete_table(u'venueregistration_userprofile')

        # Deleting model 'OTP'
        db.delete_table(u'venueregistration_otp')

        # Deleting model 'EventType'
        db.delete_table(u'venueregistration_eventtype')

        # Deleting model 'EventSubType'
        db.delete_table(u'venueregistration_eventsubtype')

        # Deleting model 'Country'
        db.delete_table(u'venueregistration_country')

        # Deleting model 'State'
        db.delete_table(u'venueregistration_state')

        # Deleting model 'City'
        db.delete_table(u'venueregistration_city')

        # Deleting model 'Location'
        db.delete_table(u'venueregistration_location')

        # Deleting model 'SeatingOption'
        db.delete_table(u'venueregistration_seatingoption')

        # Deleting model 'VenueType'
        db.delete_table(u'venueregistration_venuetype')

        # Deleting model 'CapacityRange'
        db.delete_table(u'venueregistration_capacityrange')

        # Deleting model 'MealType'
        db.delete_table(u'venueregistration_mealtype')

        # Deleting model 'MealItemCategory'
        db.delete_table(u'venueregistration_mealitemcategory')

        # Deleting model 'MealTypePackage'
        db.delete_table(u'venueregistration_mealtypepackage')

        # Deleting model 'MealCategoryPackage'
        db.delete_table(u'venueregistration_mealcategorypackage')

        # Deleting model 'MealPackage'
        db.delete_table(u'venueregistration_mealpackage')

        # Removing M2M table for field meal_type_package on 'MealPackage'
        db.delete_table(db.shorten_name(u'venueregistration_mealpackage_meal_type_package'))

        # Deleting model 'Venue'
        db.delete_table(u'venueregistration_venue')

        # Removing M2M table for field event_type on 'Venue'
        db.delete_table(db.shorten_name(u'venueregistration_venue_event_type'))

        # Deleting model 'VenueRating'
        db.delete_table(u'venueregistration_venuerating')

        # Deleting model 'CustomOption'
        db.delete_table(u'venueregistration_customoption')

        # Deleting model 'VenueSupportsEventType'
        db.delete_table(u'venueregistration_venuesupportseventtype')

        # Deleting model 'VenuePicture'
        db.delete_table(u'venueregistration_venuepicture')

        # Deleting model 'VenuePackage'
        db.delete_table(u'venueregistration_venuepackage')

        # Removing M2M table for field meal_package on 'VenuePackage'
        db.delete_table(db.shorten_name(u'venueregistration_venuepackage_meal_package'))

        # Removing M2M table for field event_type on 'VenuePackage'
        db.delete_table(db.shorten_name(u'venueregistration_venuepackage_event_type'))

        # Deleting model 'VenuePricePerItem'
        db.delete_table(u'venueregistration_venuepriceperitem')

        # Deleting model 'VenueBookingHistory'
        db.delete_table(u'venueregistration_venuebookinghistory')

        # Deleting model 'Quote'
        db.delete_table(u'venueregistration_quote')

        # Removing M2M table for field venue on 'Quote'
        db.delete_table(db.shorten_name(u'venueregistration_quote_venue'))

        # Removing M2M table for field meal_selections on 'Quote'
        db.delete_table(db.shorten_name(u'venueregistration_quote_meal_selections'))


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'venueregistration.capacityrange': {
            'Meta': {'object_name': 'CapacityRange'},
            'display_string': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'maximum': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'minimum': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'venueregistration.city': {
            'Meta': {'object_name': 'City'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.State']"})
        },
        u'venueregistration.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'venueregistration.customoption': {
            'Meta': {'object_name': 'CustomOption'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'venue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Venue']"})
        },
        u'venueregistration.eventsubtype': {
            'Meta': {'object_name': 'EventSubType'},
            'event_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.EventType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'venueregistration.eventtype': {
            'Meta': {'object_name': 'EventType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'venueregistration.location': {
            'Meta': {'object_name': 'Location'},
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.City']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'venueregistration.mealcategorypackage': {
            'Meta': {'object_name': 'MealCategoryPackage'},
            'duration': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meal_category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.MealItemCategory']"}),
            'meal_type_package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.MealTypePackage']"}),
            'number_of_non_veg_items': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'number_of_veg_items': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'sea_food_in_non_veg': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'venueregistration.mealitemcategory': {
            'Meta': {'object_name': 'MealItemCategory'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'venueregistration.mealpackage': {
            'Meta': {'object_name': 'MealPackage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meal_type_package': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['venueregistration.MealTypePackage']", 'symmetrical': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'venueregistration.mealtype': {
            'Meta': {'object_name': 'MealType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'venueregistration.mealtypepackage': {
            'Meta': {'object_name': 'MealTypePackage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meal_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.MealType']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'venueregistration.otp': {
            'Meta': {'object_name': 'OTP'},
            'expiry_date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'password': ('django.db.models.fields.IntegerField', [], {}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'venueregistration.quote': {
            'Meta': {'object_name': 'Quote'},
            'date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meal_package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.MealPackage']", 'null': 'True', 'blank': 'True'}),
            'meal_selections': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['venueregistration.MealTypePackage']", 'null': 'True', 'blank': 'True'}),
            'time': ('django.db.models.fields.TimeField', [], {}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'venue': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['venueregistration.Venue']", 'symmetrical': 'False'})
        },
        u'venueregistration.seatingoption': {
            'Meta': {'object_name': 'SeatingOption'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'venueregistration.state': {
            'Meta': {'object_name': 'State'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Country']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'venueregistration.userprofile': {
            'Meta': {'object_name': 'UserProfile'},
            'company': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mobile_number': ('django.db.models.fields.BigIntegerField', [], {}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'})
        },
        u'venueregistration.venue': {
            'Meta': {'object_name': 'Venue'},
            'capacity': ('django.db.models.fields.IntegerField', [], {}),
            'created_at': ('django.db.models.fields.DateField', [], {}),
            'description': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'event_type': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['venueregistration.EventType']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_pure_veg': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_modified_at': ('django.db.models.fields.DateField', [], {}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Location']"}),
            'manager': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'minimum_occupancy': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'rating': ('django.db.models.fields.DecimalField', [], {'default': '3.0', 'max_digits': '2', 'decimal_places': '1'}),
            'serves_liquor': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'venue_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.VenueType']"})
        },
        u'venueregistration.venuebookinghistory': {
            'Meta': {'object_name': 'VenueBookingHistory'},
            'date': ('django.db.models.fields.DateField', [], {}),
            'end_time': ('django.db.models.fields.TimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'start_time': ('django.db.models.fields.TimeField', [], {}),
            'venue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Venue']"})
        },
        u'venueregistration.venuepackage': {
            'Meta': {'object_name': 'VenuePackage'},
            'event_type': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['venueregistration.EventType']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meal_package': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['venueregistration.MealTypePackage']", 'symmetrical': 'False'}),
            'price_per_head': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'venue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Venue']"})
        },
        u'venueregistration.venuepicture': {
            'Meta': {'object_name': 'VenuePicture'},
            'displayable': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'is_display_pic': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'venue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Venue']"})
        },
        u'venueregistration.venuepriceperitem': {
            'Meta': {'object_name': 'VenuePricePerItem'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'menu_category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.MealItemCategory']"}),
            'price_per_item': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'venue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Venue']"})
        },
        u'venueregistration.venuerating': {
            'Meta': {'object_name': 'VenueRating'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'rating': ('django.db.models.fields.DecimalField', [], {'default': '3.0', 'max_digits': '2', 'decimal_places': '1'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'venue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Venue']"})
        },
        u'venueregistration.venuesupportseventtype': {
            'Meta': {'object_name': 'VenueSupportsEventType'},
            'event_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.EventType']"}),
            'evnet_subtype_id': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'venue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Venue']"})
        },
        u'venueregistration.venuetype': {
            'Meta': {'object_name': 'VenueType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        }
    }

    complete_apps = ['venueregistration']