# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'MealPackage'
        db.delete_table(u'venueregistration_mealpackage')

        # Removing M2M table for field meal_type_package on 'MealPackage'
        db.delete_table(db.shorten_name(u'venueregistration_mealpackage_meal_type_package'))

        # Deleting model 'VenuePackage'
        db.delete_table(u'venueregistration_venuepackage')

        # Removing M2M table for field meal_package on 'VenuePackage'
        db.delete_table(db.shorten_name(u'venueregistration_venuepackage_meal_package'))

        # Removing M2M table for field event_type on 'VenuePackage'
        db.delete_table(db.shorten_name(u'venueregistration_venuepackage_event_type'))

        # Deleting model 'MealCategoryPackage'
        db.delete_table(u'venueregistration_mealcategorypackage')

        # Deleting model 'MealItemCategory'
        db.delete_table(u'venueregistration_mealitemcategory')

        # Deleting model 'MealTypePackage'
        db.delete_table(u'venueregistration_mealtypepackage')

        # Deleting model 'VenuePricePerItem'
        db.delete_table(u'venueregistration_venuepriceperitem')

        # Adding model 'Package'
        db.create_table(u'venueregistration_package', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default=None, max_length=255, null=True, blank=True)),
            ('custom', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'venueregistration', ['Package'])

        # Adding M2M table for field package_category_item_option on 'Package'
        m2m_table_name = db.shorten_name(u'venueregistration_package_package_category_item_option')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('package', models.ForeignKey(orm[u'venueregistration.package'], null=False)),
            ('mealitemoption', models.ForeignKey(orm[u'venueregistration.mealitemoption'], null=False))
        ))
        db.create_unique(m2m_table_name, ['package_id', 'mealitemoption_id'])

        # Adding model 'MealOption'
        db.create_table(u'venueregistration_mealoption', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'venueregistration', ['MealOption'])

        # Adding model 'PackageDescription'
        db.create_table(u'venueregistration_packagedescription', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('package', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.Package'])),
            ('description', self.gf('django.db.models.fields.TextField')(default='No Description')),
            ('display_order', self.gf('django.db.models.fields.IntegerField')(default=1)),
        ))
        db.send_create_signal(u'venueregistration', ['PackageDescription'])

        # Adding model 'MealItem'
        db.create_table(u'venueregistration_mealitem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'venueregistration', ['MealItem'])

        # Adding M2M table for field category_item on 'MealItem'
        m2m_table_name = db.shorten_name(u'venueregistration_mealitem_category_item')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('mealitem', models.ForeignKey(orm[u'venueregistration.mealitem'], null=False)),
            ('mealcategory', models.ForeignKey(orm[u'venueregistration.mealcategory'], null=False))
        ))
        db.create_unique(m2m_table_name, ['mealitem_id', 'mealcategory_id'])

        # Adding model 'MealCategory'
        db.create_table(u'venueregistration_mealcategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal(u'venueregistration', ['MealCategory'])

        # Adding model 'MealItemOption'
        db.create_table(u'venueregistration_mealitemoption', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('meal_item', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.MealItem'])),
            ('meal_option', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.MealOption'])),
        ))
        db.send_create_signal(u'venueregistration', ['MealItemOption'])

        # Adding field 'UserProfile.is_verified'
        db.add_column(u'venueregistration_userprofile', 'is_verified',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Deleting field 'MealType.name'
        db.delete_column(u'venueregistration_mealtype', 'name')

        # Deleting field 'Quote.meal_package'
        db.delete_column(u'venueregistration_quote', 'meal_package_id')

        # Deleting field 'Quote.time'
        db.delete_column(u'venueregistration_quote', 'time')

        # Deleting field 'Quote.date'
        db.delete_column(u'venueregistration_quote', 'date')

        # Adding field 'Quote.package'
        db.add_column(u'venueregistration_quote', 'package',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.Package'], null=True),
                      keep_default=False)

        # Adding field 'Quote.quote_key'
        db.add_column(u'venueregistration_quote', 'quote_key',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Quote.guests'
        db.add_column(u'venueregistration_quote', 'guests',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['venueregistration.CapacityRange']),
                      keep_default=False)

        # Adding field 'Quote.venuetype'
        db.add_column(u'venueregistration_quote', 'venuetype',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['venueregistration.VenueType']),
                      keep_default=False)

        # Adding field 'Quote.occasion'
        db.add_column(u'venueregistration_quote', 'occasion',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['venueregistration.EventType']),
                      keep_default=False)

        # Adding field 'Quote.created_at'
        db.add_column(u'venueregistration_quote', 'created_at',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, default=datetime.datetime(2013, 8, 21, 0, 0), blank=True),
                      keep_default=False)

        # Adding field 'Quote.updated_at'
        db.add_column(u'venueregistration_quote', 'updated_at',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now=True, default=datetime.datetime(2013, 8, 21, 0, 0), blank=True),
                      keep_default=False)

        # Adding field 'Quote.date_of_booking'
        db.add_column(u'venueregistration_quote', 'date_of_booking',
                      self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2013, 8, 21, 0, 0)),
                      keep_default=False)

        # Adding field 'Quote.time_of_booking'
        db.add_column(u'venueregistration_quote', 'time_of_booking',
                      self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2013, 8, 21, 0, 0)),
                      keep_default=False)

        # Adding field 'Quote.status'
        db.add_column(u'venueregistration_quote', 'status',
                      self.gf('django.db.models.fields.CharField')(default='0', max_length=255),
                      keep_default=False)

        # Removing M2M table for field venue on 'Quote'
        db.delete_table(db.shorten_name(u'venueregistration_quote_venue'))

        # Removing M2M table for field meal_selections on 'Quote'
        db.delete_table(db.shorten_name(u'venueregistration_quote_meal_selections'))


    def backwards(self, orm):
        # Adding model 'MealPackage'
        db.create_table(u'venueregistration_mealpackage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
        ))
        db.send_create_signal(u'venueregistration', ['MealPackage'])

        # Adding M2M table for field meal_type_package on 'MealPackage'
        m2m_table_name = db.shorten_name(u'venueregistration_mealpackage_meal_type_package')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('mealpackage', models.ForeignKey(orm[u'venueregistration.mealpackage'], null=False)),
            ('mealtypepackage', models.ForeignKey(orm[u'venueregistration.mealtypepackage'], null=False))
        ))
        db.create_unique(m2m_table_name, ['mealpackage_id', 'mealtypepackage_id'])

        # Adding model 'VenuePackage'
        db.create_table(u'venueregistration_venuepackage', (
            ('price_per_head', self.gf('django.db.models.fields.DecimalField')(max_digits=6, decimal_places=2)),
            ('venue', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.Venue'])),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'venueregistration', ['VenuePackage'])

        # Adding M2M table for field meal_package on 'VenuePackage'
        m2m_table_name = db.shorten_name(u'venueregistration_venuepackage_meal_package')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('venuepackage', models.ForeignKey(orm[u'venueregistration.venuepackage'], null=False)),
            ('mealtypepackage', models.ForeignKey(orm[u'venueregistration.mealtypepackage'], null=False))
        ))
        db.create_unique(m2m_table_name, ['venuepackage_id', 'mealtypepackage_id'])

        # Adding M2M table for field event_type on 'VenuePackage'
        m2m_table_name = db.shorten_name(u'venueregistration_venuepackage_event_type')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('venuepackage', models.ForeignKey(orm[u'venueregistration.venuepackage'], null=False)),
            ('eventtype', models.ForeignKey(orm[u'venueregistration.eventtype'], null=False))
        ))
        db.create_unique(m2m_table_name, ['venuepackage_id', 'eventtype_id'])

        # Adding model 'MealCategoryPackage'
        db.create_table(u'venueregistration_mealcategorypackage', (
            ('number_of_veg_items', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('meal_category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.MealItemCategory'])),
            ('sea_food_in_non_veg', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('meal_type_package', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.MealTypePackage'])),
            ('duration', self.gf('django.db.models.fields.IntegerField')(default=None, null=True, blank=True)),
            ('number_of_non_veg_items', self.gf('django.db.models.fields.IntegerField')(default=0)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'venueregistration', ['MealCategoryPackage'])

        # Adding model 'MealItemCategory'
        db.create_table(u'venueregistration_mealitemcategory', (
            ('description', self.gf('django.db.models.fields.CharField')(max_length=1024, null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'venueregistration', ['MealItemCategory'])

        # Adding model 'MealTypePackage'
        db.create_table(u'venueregistration_mealtypepackage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('meal_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.MealType'])),
        ))
        db.send_create_signal(u'venueregistration', ['MealTypePackage'])

        # Adding model 'VenuePricePerItem'
        db.create_table(u'venueregistration_venuepriceperitem', (
            ('price_per_item', self.gf('django.db.models.fields.DecimalField')(max_digits=6, decimal_places=2)),
            ('venue', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.Venue'])),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('menu_category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.MealItemCategory'])),
        ))
        db.send_create_signal(u'venueregistration', ['VenuePricePerItem'])

        # Deleting model 'Package'
        db.delete_table(u'venueregistration_package')

        # Removing M2M table for field package_category_item_option on 'Package'
        db.delete_table(db.shorten_name(u'venueregistration_package_package_category_item_option'))

        # Deleting model 'MealOption'
        db.delete_table(u'venueregistration_mealoption')

        # Deleting model 'PackageDescription'
        db.delete_table(u'venueregistration_packagedescription')

        # Deleting model 'MealItem'
        db.delete_table(u'venueregistration_mealitem')

        # Removing M2M table for field category_item on 'MealItem'
        db.delete_table(db.shorten_name(u'venueregistration_mealitem_category_item'))

        # Deleting model 'MealCategory'
        db.delete_table(u'venueregistration_mealcategory')

        # Deleting model 'MealItemOption'
        db.delete_table(u'venueregistration_mealitemoption')

        # Deleting field 'UserProfile.is_verified'
        db.delete_column(u'venueregistration_userprofile', 'is_verified')

        # Adding field 'MealType.name'
        db.add_column(u'venueregistration_mealtype', 'name',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=128),
                      keep_default=False)

        # Adding field 'Quote.meal_package'
        db.add_column(u'venueregistration_quote', 'meal_package',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['venueregistration.MealPackage'], null=True, blank=True),
                      keep_default=False)

        # Adding field 'Quote.time'
        db.add_column(u'venueregistration_quote', 'time',
                      self.gf('django.db.models.fields.TimeField')(default=datetime.datetime(2013, 8, 21, 0, 0)),
                      keep_default=False)

        # Adding field 'Quote.date'
        db.add_column(u'venueregistration_quote', 'date',
                      self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2013, 8, 21, 0, 0)),
                      keep_default=False)

        # Deleting field 'Quote.package'
        db.delete_column(u'venueregistration_quote', 'package_id')

        # Deleting field 'Quote.quote_key'
        db.delete_column(u'venueregistration_quote', 'quote_key')

        # Deleting field 'Quote.guests'
        db.delete_column(u'venueregistration_quote', 'guests_id')

        # Deleting field 'Quote.venuetype'
        db.delete_column(u'venueregistration_quote', 'venuetype_id')

        # Deleting field 'Quote.occasion'
        db.delete_column(u'venueregistration_quote', 'occasion_id')

        # Deleting field 'Quote.created_at'
        db.delete_column(u'venueregistration_quote', 'created_at')

        # Deleting field 'Quote.updated_at'
        db.delete_column(u'venueregistration_quote', 'updated_at')

        # Deleting field 'Quote.date_of_booking'
        db.delete_column(u'venueregistration_quote', 'date_of_booking')

        # Deleting field 'Quote.time_of_booking'
        db.delete_column(u'venueregistration_quote', 'time_of_booking')

        # Deleting field 'Quote.status'
        db.delete_column(u'venueregistration_quote', 'status')

        # Adding M2M table for field venue on 'Quote'
        m2m_table_name = db.shorten_name(u'venueregistration_quote_venue')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('quote', models.ForeignKey(orm[u'venueregistration.quote'], null=False)),
            ('venue', models.ForeignKey(orm[u'venueregistration.venue'], null=False))
        ))
        db.create_unique(m2m_table_name, ['quote_id', 'venue_id'])

        # Adding M2M table for field meal_selections on 'Quote'
        m2m_table_name = db.shorten_name(u'venueregistration_quote_meal_selections')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('quote', models.ForeignKey(orm[u'venueregistration.quote'], null=False)),
            ('mealtypepackage', models.ForeignKey(orm[u'venueregistration.mealtypepackage'], null=False))
        ))
        db.create_unique(m2m_table_name, ['quote_id', 'mealtypepackage_id'])


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'venueregistration.capacityrange': {
            'Meta': {'object_name': 'CapacityRange'},
            'display_string': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'maximum': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'minimum': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'venueregistration.city': {
            'Meta': {'object_name': 'City'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.State']"})
        },
        u'venueregistration.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'venueregistration.customoption': {
            'Meta': {'object_name': 'CustomOption'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'venue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Venue']"})
        },
        u'venueregistration.eventsubtype': {
            'Meta': {'object_name': 'EventSubType'},
            'event_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.EventType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'venueregistration.eventtype': {
            'Meta': {'object_name': 'EventType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'venueregistration.location': {
            'Meta': {'object_name': 'Location'},
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.City']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'venueregistration.mealcategory': {
            'Meta': {'object_name': 'MealCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'venueregistration.mealitem': {
            'Meta': {'object_name': 'MealItem'},
            'category_item': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['venueregistration.MealCategory']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'venueregistration.mealitemoption': {
            'Meta': {'object_name': 'MealItemOption'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meal_item': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.MealItem']"}),
            'meal_option': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.MealOption']"})
        },
        u'venueregistration.mealoption': {
            'Meta': {'object_name': 'MealOption'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'venueregistration.mealtype': {
            'Meta': {'object_name': 'MealType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'venueregistration.otp': {
            'Meta': {'object_name': 'OTP'},
            'expiry_date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'password': ('django.db.models.fields.IntegerField', [], {}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'venueregistration.package': {
            'Meta': {'object_name': 'Package'},
            'custom': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'package_category_item_option': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['venueregistration.MealItemOption']", 'symmetrical': 'False'})
        },
        u'venueregistration.packagedescription': {
            'Meta': {'object_name': 'PackageDescription'},
            'description': ('django.db.models.fields.TextField', [], {'default': "'No Description'"}),
            'display_order': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Package']"})
        },
        u'venueregistration.quote': {
            'Meta': {'object_name': 'Quote'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_of_booking': ('django.db.models.fields.DateField', [], {}),
            'guests': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['venueregistration.CapacityRange']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'occasion': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['venueregistration.EventType']"}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Package']", 'null': 'True'}),
            'quote_key': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'0'", 'max_length': '255'}),
            'time_of_booking': ('django.db.models.fields.DateTimeField', [], {}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'venuetype': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['venueregistration.VenueType']"})
        },
        u'venueregistration.seatingoption': {
            'Meta': {'object_name': 'SeatingOption'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'venueregistration.state': {
            'Meta': {'object_name': 'State'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Country']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'venueregistration.userprofile': {
            'Meta': {'object_name': 'UserProfile'},
            'company': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_verified': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mobile_number': ('django.db.models.fields.BigIntegerField', [], {}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'})
        },
        u'venueregistration.venue': {
            'Meta': {'object_name': 'Venue'},
            'address1': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'address2': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'address3': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'base_package_rate': ('django.db.models.fields.DecimalField', [], {'default': '10000.0', 'max_digits': '10', 'decimal_places': '2'}),
            'capacity': ('django.db.models.fields.IntegerField', [], {}),
            'created_at': ('django.db.models.fields.DateField', [], {}),
            'description': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'event_type': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['venueregistration.EventType']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_pure_veg': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_modified_at': ('django.db.models.fields.DateField', [], {}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Location']"}),
            'manager': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'minimum_occupancy': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'rating': ('django.db.models.fields.DecimalField', [], {'default': '3.0', 'max_digits': '2', 'decimal_places': '1'}),
            'serves_liquor': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'venue_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.VenueType']"})
        },
        u'venueregistration.venuebookinghistory': {
            'Meta': {'object_name': 'VenueBookingHistory'},
            'date': ('django.db.models.fields.DateField', [], {}),
            'end_time': ('django.db.models.fields.TimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'start_time': ('django.db.models.fields.TimeField', [], {}),
            'venue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Venue']"})
        },
        u'venueregistration.venuepicture': {
            'Meta': {'object_name': 'VenuePicture'},
            'displayable': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'is_display_pic': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'venue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Venue']"})
        },
        u'venueregistration.venuerating': {
            'Meta': {'object_name': 'VenueRating'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'rating': ('django.db.models.fields.DecimalField', [], {'default': '3.0', 'max_digits': '2', 'decimal_places': '1'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'venue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Venue']"})
        },
        u'venueregistration.venuesupportseventtype': {
            'Meta': {'object_name': 'VenueSupportsEventType'},
            'event_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.EventType']"}),
            'evnet_subtype_id': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'venue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Venue']"})
        },
        u'venueregistration.venuetype': {
            'Meta': {'object_name': 'VenueType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        }
    }

    complete_apps = ['venueregistration']