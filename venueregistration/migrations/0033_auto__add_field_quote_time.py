# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Quote.time'
        db.add_column(u'venueregistration_quote', 'time',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['venueregistration.BookingTimeType']),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Quote.time'
        db.delete_column(u'venueregistration_quote', 'time_id')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'venueregistration.bookingtimetype': {
            'Meta': {'object_name': 'BookingTimeType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'venueregistration.capacityrange': {
            'Meta': {'object_name': 'CapacityRange'},
            'display_string': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'maximum': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'minimum': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'venueregistration.city': {
            'Meta': {'object_name': 'City'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.State']"})
        },
        u'venueregistration.contact': {
            'Meta': {'object_name': 'Contact'},
            'company': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mobile': ('django.db.models.fields.BigIntegerField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'venueregistration.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'venueregistration.customoption': {
            'Meta': {'object_name': 'CustomOption'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'venue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Venue']"})
        },
        u'venueregistration.eventsubtype': {
            'Meta': {'object_name': 'EventSubType'},
            'event_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.EventType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'venueregistration.eventtype': {
            'Meta': {'object_name': 'EventType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'venueregistration.favourite': {
            'Meta': {'object_name': 'Favourite'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'venue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Venue']"})
        },
        u'venueregistration.location': {
            'Meta': {'object_name': 'Location'},
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.City']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'venueregistration.mealcategory': {
            'Meta': {'object_name': 'MealCategory'},
            'how_to': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'venueregistration.mealcategoryitem': {
            'Meta': {'object_name': 'MealCategoryItem'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meal_category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.MealCategory']"}),
            'meal_item': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.MealItem']"})
        },
        u'venueregistration.mealcategoryitemoption': {
            'Meta': {'object_name': 'MealCategoryItemOption'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meal_category_item': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.MealCategoryItem']"}),
            'meal_option': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.MealOption']"})
        },
        u'venueregistration.mealitem': {
            'Meta': {'object_name': 'MealItem'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'venueregistration.mealoption': {
            'Meta': {'object_name': 'MealOption'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'venueregistration.mealtype': {
            'Meta': {'object_name': 'MealType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'venueregistration.otp': {
            'Meta': {'object_name': 'OTP'},
            'expiry_date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'venueregistration.package': {
            'Meta': {'object_name': 'Package'},
            'custom': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meal_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.MealType']"}),
            'name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'venueregistration.packagecontents': {
            'Meta': {'object_name': 'PackageContents'},
            'contents': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.MealCategoryItemOption']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Package']", 'null': 'True'})
        },
        u'venueregistration.packagedescription': {
            'Meta': {'object_name': 'PackageDescription'},
            'description': ('django.db.models.fields.TextField', [], {'default': "'No Description'"}),
            'display_order': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Package']"})
        },
        u'venueregistration.quote': {
            'Meta': {'object_name': 'Quote'},
            'date_of_booking': ('django.db.models.fields.DateTimeField', [], {}),
            'guests': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['venueregistration.CapacityRange']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'occasion': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['venueregistration.EventType']", 'null': 'True', 'blank': 'True'}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Package']", 'null': 'True'}),
            'quote_key': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'0'", 'max_length': '255'}),
            'time': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.BookingTimeType']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'venuetype': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['venueregistration.VenueType']", 'null': 'True', 'blank': 'True'})
        },
        u'venueregistration.quotevenue': {
            'Meta': {'object_name': 'QuoteVenue'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'quote': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Quote']"}),
            'venue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Venue']"})
        },
        u'venueregistration.recentlyviewed': {
            'Meta': {'object_name': 'RecentlyViewed'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'venue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Venue']"})
        },
        u'venueregistration.relatedvenues': {
            'Meta': {'object_name': 'RelatedVenues'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'related_venue': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'related_venue'", 'to': u"orm['venueregistration.Venue']"}),
            'venue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Venue']"})
        },
        u'venueregistration.seatingoption': {
            'Meta': {'object_name': 'SeatingOption'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'venueregistration.state': {
            'Meta': {'object_name': 'State'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Country']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'venueregistration.suggestvenue': {
            'Meta': {'object_name': 'SuggestVenue'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'venue': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'venueregistration.userprofile': {
            'Meta': {'object_name': 'UserProfile'},
            'company': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_verified': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mobile_number': ('django.db.models.fields.BigIntegerField', [], {}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'})
        },
        u'venueregistration.venue': {
            'Meta': {'object_name': 'Venue'},
            'address1': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'address2': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'address3': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'base_package_rate': ('django.db.models.fields.DecimalField', [], {'default': '10000.0', 'max_digits': '10', 'decimal_places': '2'}),
            'capacity': ('django.db.models.fields.IntegerField', [], {}),
            'cost_per_head': ('django.db.models.fields.DecimalField', [], {'default': '2500.0', 'max_digits': '10', 'decimal_places': '2'}),
            'created_at': ('django.db.models.fields.DateField', [], {}),
            'description': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'event_type': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['venueregistration.EventType']", 'symmetrical': 'False'}),
            'extra_content': ('django.db.models.fields.TextField', [], {'default': "'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ipsum velit, porta a orci gravida, tempor placerat leo. Vestibulum eu mauris in erat pulvinar fermentum. Donec quis lacus eu risus congue fermentum. Curabitur scelerisque odio ac pretium placerat. Donec convallis dapibus nunc id sagittis. Donec ut scelerisque urna. Nullam egestas semper est, sed lacinia elit ullamcorper sit amet.</p><p>Suspendisse nec sagittis tortor, ac ullamcorper lorem. Aenean vitae augue et quam pretium accumsan. Donec elementum molestie nunc, sit amet molestie magna sagittis ut. Aenean odio risus, faucibus quis gravida laoreet, scelerisque eu arcu. Donec nisi dui, facilisis id dictum et, faucibus sit amet nisi. Fusce nec erat leo. Nulla facilisi.</p>'"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_pure_veg': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_modified_at': ('django.db.models.fields.DateField', [], {}),
            'lattitude': ('django.db.models.fields.DecimalField', [], {'default': '17.46994', 'max_digits': '10', 'decimal_places': '5'}),
            'lead_text': ('django.db.models.fields.TextField', [], {'default': "'LOREM IPSUM DOLOR SIT AMET, CONSECTETUER ADIPISCING ELIT. NAM CURSUS. MORBI UT MI. NULLAM ENIM LEO, EGESTAS ID, CONDIMENTUM AT, LAOREET MATTIS, MASSA. SED ELEIFEND NONUMMY DIAM. PRAESENT MAURIS ANTE,'"}),
            'lighting': ('django.db.models.fields.TextField', [], {'default': "'LED Lights'"}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Location']"}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'default': '78.50757', 'max_digits': '10', 'decimal_places': '5'}),
            'manager': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'minimum_occupancy': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'music': ('django.db.models.fields.TextField', [], {'default': "'Western Classical'"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'rating': ('django.db.models.fields.DecimalField', [], {'default': '3.0', 'max_digits': '2', 'decimal_places': '1'}),
            'review_count': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'serves_liquor': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'service': ('django.db.models.fields.TextField', [], {'default': "'Bar, Buffet'"}),
            'venue_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.VenueType']"})
        },
        u'venueregistration.venuebookinghistory': {
            'Meta': {'object_name': 'VenueBookingHistory'},
            'booking_time': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.BookingTimeType']"}),
            'date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'venue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Venue']"})
        },
        u'venueregistration.venuepicture': {
            'Meta': {'object_name': 'VenuePicture'},
            'displayable': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'is_display_pic': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'venue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Venue']"})
        },
        u'venueregistration.venuerating': {
            'Meta': {'object_name': 'VenueRating'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'rating': ('django.db.models.fields.DecimalField', [], {'default': '3.0', 'max_digits': '2', 'decimal_places': '1'}),
            'review': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'venue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Venue']"})
        },
        u'venueregistration.venuesupportseventtype': {
            'Meta': {'object_name': 'VenueSupportsEventType'},
            'event_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.EventType']"}),
            'evnet_subtype_id': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'venue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['venueregistration.Venue']"})
        },
        u'venueregistration.venuetype': {
            'Meta': {'object_name': 'VenueType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        }
    }

    complete_apps = ['venueregistration']