from django import template
import collections

from django.core.serializers import serialize
from django.db.models.query import QuerySet
from django.utils import simplejson


register = template.Library()

@register.filter(name='key_val')
def key_val(d,args):
    current_dict = d
    if args is None:
        return False
    arg_list = [arg.strip() for arg in args.split(',')]
    for arg in arg_list:
        if type(current_dict) is collections.OrderedDict:
            current_dict = current_dict.get(to_num(arg), '')
        else:
            current_dict =  current_dict.__dict__[arg]    
    return str (current_dict)

def to_num(inputNum):
    ind = inputNum
    try:
        return int(ind)
    except:
        return ind   



@register.filter(name='mult')
def mult(value,args):
    "Multiplies the arg and the value"
    return round(value*args,2)


@register.filter(name='div')
def div(value,args):
    "Divides the value by the args"
    return round(value/args,2)


@register.filter(name='jsonify')
def jsonify(object):
    if isinstance(object, QuerySet):
        return serialize('json', object)
    return simplejson.dumps(object)