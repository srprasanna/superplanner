from django.conf.urls import patterns, url

from venueregistration.views import CityLocationsView, PopularVenuesOfCityView, SingupView, login_view, logout_view,SearchView,SearchAllView,VenueDetailsView,ShortListView,ConfirmationView,send_sms,VerifyView,FoodAndBeveragesView,CancelPackageView,UpdatePackageView,SubmitReviewView,ContactSubmitView,ContactView,AboutView,PopularVenuesView,SuggestVenuesView,SetFavourteVenueView,GetFavouriteVenueView,ForgotPasswordView,MyAccountView,UpdateContactView,ResendOTPView,UpdatePasswordView,ImageRenderer,ShowQuoteView
from venueregistration.models import Contact
from django.views.generic import TemplateView
urlpatterns = patterns('',
    #url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^city/(?P<pk>\d+)/locations/$', CityLocationsView.as_view(), name='city_locations'),
    url(r'^city/(?P<pk>\d+)/popularvenues/$', PopularVenuesOfCityView.as_view(), name='popular_venues'),
    url(r'^signup/$', SingupView.as_view(), name="signup"),
    url(r'^login/$', login_view, name="login"),
    url(r'^logout/$', logout_view, name="logout"),
    url(r'^search/$',SearchView.as_view(),name='search_venues'),
    url(r'^getallvenues/$',SearchAllView.as_view(),name='search_all_venues'),
    url(r'^venue/$',VenueDetailsView.as_view(),name='venue_details'),
    url(r'^shortlist/$',ShortListView.as_view(),name='shortlist'),
    url(r'^confirmation/$',ConfirmationView.as_view(),name='confirmation'),
    url(r'^send_sms/$',send_sms,name='send_sms'),
    url(r'^verify_user/$',VerifyView.as_view(),name='verify_view'),
    url(r'^quote/addfoodbev$',FoodAndBeveragesView.as_view(),name='add_fb_quote'),
    url(r'^quote/removefoodbev$',FoodAndBeveragesView.as_view(),name='remove_fb_quote'),
    url(r'^quote/updatepackage$',UpdatePackageView.as_view(),name='update_fb_quote'),
    url(r'^quote/cancelfoodbev$',CancelPackageView.as_view(),name='cancel_fb_quote'),
    url(r'^submitreview',SubmitReviewView.as_view(),name='submit_review'),
    url(r'^contact/$', ContactView.as_view(),name="contact"),
    url(r'^contactsubmit/$',ContactSubmitView.as_view(),name='contact_submit'),
    url(r'^about/$',AboutView.as_view(),name='about'),
    url(r'^venues/popular$',PopularVenuesView.as_view(),name='popular'),
    url(r'^suggestvenue/$',SuggestVenuesView.as_view(),name='suggest'),
    url(r'^togglefavourites/$',SetFavourteVenueView.as_view(),name='favourite'),
    url(r'^getfavourites/$',GetFavouriteVenueView.as_view(),name='get_favourite'),
    url(r'^forgotpassword/$',ForgotPasswordView.as_view(),name='forgot_password'),
    url(r'^myaccount$',MyAccountView.as_view(),name='my_account'),
    url(r'^updatecontact/$',UpdateContactView.as_view(),name='update_contact'),
    url(r'^updatepassword/$',UpdatePasswordView.as_view(),name='update_password'),
    url(r'^resendotp/$',ResendOTPView.as_view(),name='resend_otp'),
    url(r'^getimage/$',ImageRenderer.as_view(),name='image_renderer'),
    url(r'^getquotedetails/$',ShowQuoteView.as_view(),name='quote_view'),
    

)