import user,os
from django.http import HttpResponse
from django.utils.formats import get_format
from django.views.generic.edit import FormView
from django.views.generic import View, TemplateView
import collections
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.shortcuts import redirect, render  # , render_to_response
from django.core.mail import send_mail
import time
from setuptools import package_index
from sms import send_sms_msg,send_mail_registration
from util import id_generator
from django.utils.dateformat import DateFormat, TimeFormat
from django.utils.formats import get_format
from location import get_client_ip,get_location
# from django.contrib.formtools.wizard.views import SessionWizardView 

import re

import simplejson, datetime,json,datetime

from venueregistration.models import (
    EventType, Location, VenueSupportsEventType, VenuePicture,
    Venue, UserProfile, CapacityRange, City, VenueBookingHistory,OTP,Quote,Package,SeatingOption,VenueType,QuoteVenue,PackageDescription,PackageContents,MealCategoryItemOption,MealCategory,MealCategoryItem,MealItem,MealOption,MealType,VenueRating,Contact,SuggestVenue,Favourite,RelatedVenues,RecentlyViewed,
    BookingTimeType)
from venueregistration.forms import SearchForm, SearchResultsForm, ShortlistForm, GetQuoteForm, FoodAndBeveragesForm,ContactForm
from superplanner.settings import SITE_ROOT




class IndexView(FormView):
    form_class = SearchForm
    template_name = 'venueregistration/index.html'


    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        # form.send_email()
        return super(IndexView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)

        event_type_list = EventType.objects.all()
        popular_venue_list = Venue.objects.order_by("-rating")[:9]
        venue_ids = [venue.id for venue in popular_venue_list ]

        popular_venue_pictures = VenuePicture.objects.filter(venue__id__in=venue_ids)
        venue_event_types = VenueSupportsEventType.objects.filter(venue__id__in=venue_ids)
        cities = City.objects.all()
        
        username = ""
        if (self.request.user is None):
            username = ""
        elif (self.request.user.is_authenticated()):
            username = self.request.user.username
        else:
            username = ""
        ip = get_client_ip(self.request)
        location = get_location(ip)
        self.form_class=SearchForm(initial={"date":datetime.datetime.now()})
        context.update({'username': username, 'event_type_list': event_type_list, 'popular_venue_list': popular_venue_list,
                        'popular_venue_pictures': popular_venue_pictures, 'venue_event_types' : venue_event_types, 'title':"Home","form_class":self.form_class,"cities":cities,"location":str(location)})
        return context

class CityLocationsView(View):

    def get(self, request, *args, **kwargs):
        city_id = kwargs.get('pk')
        locations = Location.objects.filter(city_id=city_id)
        response = HttpResponse(content_type="application/json")
        loc_list = [{'id':loc.id, 'name':loc.name} for loc in locations]
        response.write(simplejson.dumps(loc_list))
        return response

class PopularVenuesOfCityView(View):

    def get(self, request, *args, **kwargs):
        city_id = kwargs.get('pk')
        pictures = VenuePicture.objects.filter(venue__location__city_id=city_id, is_display_pic=True)
        venue_data = [{'id': pic.venue.id, 'name': pic.venue.name,
                       'picture': pic.image, 'description': pic.venue.description}
                      for pic in pictures]
        response = HttpResponse(content_type="application/json")
        response.write(simplejson.dumps(venue_data))
        return response

class SingupView(View):

    def post(self, request, *args, **kwargs):
        json_data = request.raw_post_data
        user_id=0
        data = simplejson.loads(json_data)
        email = data.get('email')
        name = data.get('name')
        company = data.get('company')
        mobile = data.get('mobile')
        status = ""
        MOBILE_REGEX = re.compile(r"^[789]\d{9}$")

        if email and name and mobile:
            valid_email = False

            try:
                validate_email(email)
                valid_email = True
            except ValidationError:
                valid_email = False

            if not valid_email:
                status = "Invalid Email ID"
            elif not MOBILE_REGEX.match(mobile):
                status = "Invalid mobile number"
            else:
                password = id_generator()
                try:
                    user = User.objects.create_user(username=email,
                                                    email=email,
                                                    password=password,
                                                    first_name=name)
                        # user.is_active = False
                    user.save()

                    user_profile = UserProfile.objects.create(user=user, company=company, mobile_number=mobile,is_verified=False)
                    user_profile.save()

                    status = "Success"
                    user_id = user.id
                    to = [user.email]
                    to_num = user_profile.mobile_number
                    to.append(to_num)
                    send_mail_registration(to,password)
                    otp = id_generator()
                    otp_obj = OTP.objects.create(user=user,password=otp,expiry_date=datetime.datetime.now())
                    send_sms_msg(to,otp)
                except:
                    status="Email already exists"



                # new_user = authenticate(username=email, password=password)
                # login(request, new_user)
        else:
            status = "Please enter all required fields"

        response_data = {"status": status, "user_id": user_id}
        response = HttpResponse(content_type="application/json")
        response.write(simplejson.dumps(response_data))
        return response


class VerifyView(View):
    def get(self, request, *args, **kwargs):
        pin = request.GET.get("pin")
        user_id= int(request.GET.get("user_id"))
        otp = OTP.objects.filter(user_id__exact=user_id,password__exact=pin)
        size = len(otp)
        if size == 1:
            user = User.objects.get(pk=user_id)
            user_profile = user.get_profile()
            user_profile.is_verified = True
            user_profile.save()
            status = "Success"            
        else:
            status = "Wrong OTP"

        response_data = {"status":status}
        response = HttpResponse(content_type="application/json")
        response.write(simplejson.dumps(response_data))
        return response



def send_sms(request):
    to = request.GET.get("to")
    msg = request.GET.get("msg")
    res = send_sms_msg(to,msg)
    response_data = {"status": res}
    response = HttpResponse(content_type="application/json")
    response.write(simplejson.dumps(response_data))
    return response

def login_view(request):
    json_data = request.raw_post_data
    data = simplejson.loads(json_data)
    username = data.get('username')
    password = data.get('password')
    remember_me = data.get('rememberme')
    user = authenticate(username=username, password=password)

    status = ""
    user_name = ""
    name = ""
    if user is not None:
        if user.is_active:
            login(request, user)
            status = "Success"
            user_name = user.username
            name = user.first_name
            if remember_me == 1:
                request.session.set_expiry(1209600)  # 2 weeks
        else:
            status = "Inactive"
    else:
        status = "Username / Password did not match"

    response_data = {"status": status, "name": name}
    response = HttpResponse(content_type="application/json")
    response.write(simplejson.dumps(response_data))
    return response

def logout_view(request):
    logout(request)
    return redirect('/')

# class QuoteWizardView(SessionWizardView):
#    def done(self, form_list, **kwargs):
#        form_data = process_form_data(form_list)
#        return render_to_response('venueregistration/confirmation.html', {'form_data' : form_data})

# def process_form_data(form_list):
#    form_data = [form.cleaned_data for form in form_list]
#    return form_data

class SearchAllView(FormView):    
    template_name = 'venueregistration/searchallresults.html'


    def get(self, request, *args, **kwargs):
        city = request.GET.get('city')
        selected_venue_pictures = VenuePicture.objects.filter(is_display_pic=True)
        if city is None or city =="":
            city_object = City.objects.all()
        else:
            city_object = City.objects.filter(pk=int(city))        


        self.selected_venues = Venue.objects.filter(location__city__in=city_object)
        self.selected_venue_pictures = selected_venue_pictures.filter(venue__in=self.selected_venues).order_by('-venue__rating')        
        self.selected_venues_dict = collections.OrderedDict()
        for venue in self.selected_venues:
            self.selected_venues_dict[venue.id] = venue
            
        self.selected_venues = self.selected_venues.order_by('-rating')[:6]
        return render(request, self.template_name, self.get_context_data())

    def form_valid(self, form):
        return super(SearchAllView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(SearchAllView, self).get_context_data(**kwargs)
        context.update({'similar_venues_with_pictures': self.selected_venue_pictures, 'selected_venues':self.selected_venues_dict})
        return context

class SearchView(FormView):
    form_class = SearchResultsForm
    template_name = 'venueregistration/searchresults.html'

    selected_venue_pictures = VenuePicture.objects.none()
    similar_venues_with_pictures = {}

    def get(self, request, *args, **kwargs):
        global form_class, selected_venues, selected_venue_pictures, similar_venues_with_pictures
        self.selected_venues = Venue.objects.none()
        self.similar_venues_with_pictures = {}
        path = request.get_full_path()
        request.session['search_request'] = path
        selected_venue_pictures = VenuePicture.objects.filter(is_display_pic=True)
        city = request.GET.get('city')
        try:
            city_object = City.objects.get(pk=city)
        except:
            city_object = City.objects.all()
        locality = request.GET.getlist('locality')
        now = datetime.datetime.now()
        # date = datetime.datetime.strptime(request.GET.get('date', now.strftime('%d/%m/%Y')), '%d/%m/%Y').strftime('%Y-%m-%d')

        try:
            dateNeeded = datetime.datetime.strptime(request.GET.get('date'),'%d/%m/%y').strftime("%Y-%m-%d")
        except:
            dateNeeded = now.strftime("%Y-%m-%d")


        timeNeeded = request.GET.get('time')
        guests = request.GET.get('guests', 1)
        environment = request.GET.getlist('environment')
        request.session['environment']=environment
          
        try:
            occasion = request.GET.getlist('occasion')
            occasions = EventType.objects.filter(id__in=occasion)
        except:
            occasion = 0
            occasions = EventType.objects.all()

        seating = request.GET.get("seating", 1)
        request.session['event_type'] = occasion
        request.session['seating']=seating
        request.session['guests']=guests
        request.session['date']=dateNeeded
        request.session['time']=timeNeeded
        request.session['occasion']=occasion
        
        self.form_class = SearchResultsForm(initial={'city' : city, 'locality' : locality, 'date' : datetime.datetime.strptime(dateNeeded,"%Y-%m-%d").strftime("%d/%m/%y"),
                                                     'time' : timeNeeded, 'guests' : guests, 'environment' : environment, 'seating':seating,'occasion':occasion})

        capacity_range = CapacityRange.objects.get(pk=guests)
        if environment is None or len(environment) == 0:
            environment=VenueType.objects.all()
        else:
            environment=VenueType.objects.filter(id__in=environment) 

        if locality is None or locality == '' or len(locality) == 0:
            self.selected_venues = Venue.objects.filter(location__city=city_object, capacity__gte=capacity_range.maximum,
                                                        minimum_occupancy__lte=capacity_range.minimum, venue_type__in=environment,event_type__in=occasions)
        else:
            self.selected_venues = Venue.objects.filter(location__in=locality, capacity__gte=capacity_range.maximum,
                                                        minimum_occupancy__lte=capacity_range.minimum, venue_type__in=environment,event_type__in=occasions)

        self.selected_venues = self.selected_venues.filter(event_type__in=occasions)

        try:
            venues_booked = VenueBookingHistory.objects.filter(date=dateNeeded, booking_time__in=time)
        except:
            venues_booked = VenueBookingHistory.objects.filter(date=dateNeeded)

        venue_ids = set()
        venue_ids_add = venue_ids.add
        [ venue.id for venue in venues_booked if venue.id not in venue_ids and not venue_ids_add(venue)]

        self.selected_venues = self.selected_venues.exclude(id__in=venue_ids)
        # selected_venues = selected_venues.order_by('-rating')[:6]

        self.selected_venue_pictures = selected_venue_pictures.filter(venue__in=self.selected_venues).order_by('-venue__rating')

        for venue in selected_venue_pictures:
            self.similar_venues_with_pictures[venue.venue.id] = selected_venue_pictures.get(venue=venue.venue)
        self.selected_venues_dict = collections.OrderedDict()
        for venue in self.selected_venues:
            self.selected_venues_dict[venue.id] = venue
        self.selected_venues = self.selected_venues.order_by('-rating')[:6]

        return render(request, self.template_name, self.get_context_data())

    def form_valid(self, form):
        return super(SearchView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(SearchView, self).get_context_data(**kwargs)
        username = ""
        if (self.request.user is None):
            username = ""
        elif (self.request.user.is_authenticated()):
            username = self.request.user.username
        else:
            username = ""

        context.update({'similar_venues_with_pictures': self.selected_venue_pictures, 'form_class':self.form_class, 'username':username, 'selected_venues':self.selected_venues_dict})
        return context

class CompareView(FormView):
    form_class = ShortlistForm
    template_name = 'venueregistration/comparevenues.html'


    def get(self, request, *args, **kwargs):
        global selected_venues, selected_venue_pictures, similar_venues_with_pictures
        self.selected_venues = Venue.objects.none()
        self.selected_venue_pictures = VenuePicture.objects.filter(is_display_pic=True)
        self.similar_venues_with_pictures = {}
        json_data = request.GET.get('venue_ids', '')
        venue_ids = simplejson.loads(json_data)

        self.selected_venues = self.selected_venues.filter(id__in=venue_ids)
        self.selected_venue_pictures = self.selected_venue_pictures.filter(venue__in=self.selected_venues)

        for venue in self.similar_venues:
            self.similar_venues_with_pictures[venue] = self.similar_venue_pictures.get(venue=venue)
        return render(request, self.template_name, self.get_context_data())

    def form_valid(self, form):
        return super(CompareView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CompareView, self).get_context_data(**kwargs)

        context.update({'similar_venues_with_pictures': self.similar_venues_with_pictures})
        return context

class ShortListView(FormView):
    form_class = GetQuoteForm
    template_name = 'venueregistration/getquotes.html'


    def get(self, request, *args, **kwargs):
        self.selected_venues = Venue.objects.all()
        self.selected_venue_pictures = VenuePicture.objects.filter(is_display_pic=True)
        self.selected_venues_with_pictures = {}
        try:
            occasion = request.session['event_type']
        except:
            occasion = None
        seating = request.session['seating']
        guests = request.session['guests']
        dateUsed=str(request.session['date'])
        timeNeeded = int(request.session['time'])
        time_obj = BookingTimeType.objects.get(pk=timeNeeded)
        dateNeeded = datetime.datetime.strptime(dateUsed, '%Y-%m-%d')
        
        
        try:
            occasion = request.session['occasion']
        except:
            occasion = None
        
        environment = request.session['environment']
        try:
            guestsModel = CapacityRange.objects.filter(id=guests)[:1].get()
        except:
            guestModel = None
        try:
            occasionModel = EventType.objects.filter(id=occasion)[:1].get()
        except:
            occasionModel = None
        try:
            seatingModel = SeatingOption.objects.filter(id=seating)[:1].get()
        except:
            seatingModel = None

        try:
            venueTypeModel = VenueType.objects.filter(id=environment)[:1].get()
        except:
            venueTypeModel = None

        if(request.user.is_authenticated()):
            user = User.objects.filter(id=request.user.id)[:1].get()
        else:
            user = User.objects.filter(id=1)[:1].get()

        if(request.GET.get("quote_id") != None):
            quote_id = int(request.GET.get("quote_id"))
            self.quote = Quote.objects.get(id=quote_id)
            venue_quotes = QuoteVenue.objects.filter(quote_id = quote_id)
            venue_ids = []
            for venue_quote in venue_quotes:
                venue_ids.append(venue_quote.venue.id)
        else:
            json_data = request.GET.get('venue_ids', '')
            venue_ids = simplejson.loads(json_data)
            self.quote = Quote.objects.create(user=user,time=time_obj,package=None,quote_key=str(int(time.time())),guests=guestsModel,venuetype=venueTypeModel,occasion=occasionModel,date_of_booking=dateNeeded)
        self.guestsModel = guestsModel
        self.venue_ids = venue_ids
        self.selected_venues = self.selected_venues.filter(id__in=venue_ids)
        self.selected_venue_pictures = self.selected_venue_pictures.filter(venue__in=self.selected_venues)
        venue_names = []
        for venue in self.selected_venues:
            venue_names.append(venue.name)
            self.selected_venues_with_pictures[venue.id] = self.selected_venue_pictures.get(venue=venue)
            if(request.GET.get("quote_id") == None):
                QuoteVenue.objects.create(venue=venue,quote=self.quote)
        self.selected_venues_dict = {}
        for venue in self.selected_venues:
            self.selected_venues_dict[venue.id] = venue
        self.f_and_b = False
        if ('f_and_b' in request.session) :
            self.f_and_b = request.session['f_and_b']
            request.session['f_and_b']=False

        self.related_venues = RelatedVenues.objects.filter(venue__in=self.selected_venues)
        self.related_venues_with_pictures={}
        self.venue_string = ""
        for venue_name in venue_names:
            self.venue_string=self.venue_string+str(venue_name)+","
        for venue in self.related_venues:
            self.related_venues_with_pictures[venue.related_venue.id]=VenuePicture.objects.get(venue=venue.related_venue,is_display_pic=True)
        return render(request, self.template_name, self.get_context_data())

    def form_valid(self, form):
        return super(ShortListView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(ShortListView, self).get_context_data(**kwargs)
        username = ""
        if (self.request.user is None):
            username = ""
        elif (self.request.user.is_authenticated()):
            username = self.request.user.username
        else:
            username = ""

        self.form_class = GetQuoteForm(initial={'name':"", 'address':"", 'company':"", 'email':"", 'mobile':"", 'f_and_b':self.f_and_b})
        context.update({'venue_string':self.venue_string.rstrip(","),'related_venues_pictures':self.related_venues_with_pictures,'guests':self.guestsModel,'selected_venues_with_pictures': self.selected_venues_with_pictures, "selected_venues":self.selected_venues_dict, "username": username, "venue_ids":self.venue_ids, "form":self.form_class,"back_to_search":self.request.session["search_request"],"quote":self.quote})
        return context

class RemoveFoodAndBeveragesView(FormView):
    def get(self, request, *args, **kwargs):
        quote_id = request.GET.get("quote_id")
        quote = Quote.objects.get(id=int(quote_id))
        quote.package = None
        quote.save()
        response_data = {"status": "Deleted F&B from quote", "user_id": request.user.id}
        response = HttpResponse(content_type="application/json")
        response.write(simplejson.dumps(response_data))
        return response


class FoodAndBeveragesView(FormView):
    template_name = 'venueregistration/foodandbeverages.html'
    form_class = FoodAndBeveragesForm


    def get(self, request, *args, **kwargs):
        global selected_venues, selected_venue_pictures
        self.selected_venues = Venue.objects.none()
        quote_id = int(request.GET.get('quote_id', 0))
        self.quote = Quote.objects.get(id = quote_id)
        quoteVenues = QuoteVenue.objects.filter(quote_id=quote_id)
        self.predefinedPackages = Package.objects.filter(custom=0)
        self.packageDescription = {}
        for package in self.predefinedPackages:
            packageDescription = PackageDescription.objects.filter(package_id = package.id)
            self.packageDescription[package.id]=packageDescription
        self.menu={}
        self.mealType = MealType.objects.all()
        self.mealCategories = MealCategory.objects.all()
        self.mealItems = MealItem.objects.all()
        self.mealOptions = MealOption.objects.all()
        self.mealCategoryItem = MealCategoryItem.objects.all()
        self.mealCategoryItemOption = MealCategoryItemOption.objects.all()


        return render(request, self.template_name, self.get_context_data())

    def form_valid(self, form):
        return super(FoodAndBeveragesView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(FoodAndBeveragesView, self).get_context_data(**kwargs)
        context.update({'quote':self.quote,'mealType':self.mealType,'mealCategories': self.mealCategories,"mealItems":self.mealItems,"mealOptions":self.mealOptions,"mealCategoryItem":self.mealCategoryItem,"mealCategoryItemOption":self.mealCategoryItemOption,"packages":self.predefinedPackages,"packageDescription":self.packageDescription})
        return context

class ConfirmationView(TemplateView):
    template_name = 'venueregistration/confirmationpage.html'


    def post(self, request, *args, **kwargs):
        global quote_id, similar_venues, similar_venue_pictures, simalar_venues_with_pictures
        quote_id = int(request.POST.get("quote_id"))
        quote = Quote.objects.get(id=quote_id)
        quote_venue = QuoteVenue.objects.filter(quote=quote)
        venues = []
        for venue in quote_venue:
            venues.append(venue.venue.name)
        self.quote_id = quote.quote_key
        email = request.POST.get("email")
        # similar_venues = Venue.objects.none()
        # similar_venue_pictures = VenuePicture.objects.filter(is_display_pic=True)
        # self.similar_venues_with_pictures = {}

        event_type_id = request.session['event_type']
        try:
             event_type = EventType.objects.get(pk=event_type_id)
        except:
             event_type = EventType.objects.all()

        # similar_venues = Venue.objects.filter(event_type=event_type)
        #
        # similar_venue_pictures = similar_venue_pictures.filter(venue__in=similar_venues)
        venue_string = ""
        for venue in venues:
            venue_string=venue_string+str(venue)+","
        # for venue in similar_venues:
        #     self.similar_venues_with_pictures[venue] = similar_venue_pictures.get(venue=venue)
        send_mail("Your superplanner quote details", "You have successfully requested a quote from us for venues "+venue_string.rstrip(",")+". Your quote id is "+str(self.quote_id), "prasanna@srprasanna.com", [email], False)
        return render(request, self.template_name, self.get_context_data())

    def get_context_data(self, **kwargs):
        context = super(ConfirmationView, self).get_context_data(**kwargs)

        context.update({'quote_id': self.quote_id})
        return context

class VenueDetailsView(TemplateView):
    template_name = 'venueregistration/venuedetails.html'


    def get(self, request, *args, **kwargs):
        global selected_venue, selected_venue_pictures
        self.selected_venue = None
        self.selected_venue_pictures = VenuePicture.objects.none()
        venue_id = int(request.GET.get('venue', 1))
        self.venue_id = venue_id

        self.selected_venue = Venue.objects.get(pk=venue_id)
        self.venue_ratings = VenueRating.objects.filter(venue = self.selected_venue)
        self.selected_venue_pictures = VenuePicture.objects.filter(venue_id=venue_id)
        self.selected_venue_display_pic = VenuePicture.objects.filter(venue=self.selected_venue,is_display_pic=True)
        if request.user.is_authenticated():
            user = User.objects.get(pk=request.user.id)
            venue = Venue.objects.get(pk=venue_id)
            RecentlyViewed.objects.create(user=user,venue=venue)
            

        return render(request, self.template_name, self.get_context_data())

    def get_context_data(self, **kwargs):
        context = super(VenueDetailsView, self).get_context_data(**kwargs)
        username = ""
        if (self.request.user is None):
            username = ""
        elif (self.request.user.is_authenticated()):
            username = self.request.user.username
        else:
            username = ""

        try:
            url = self.request.session["search_request"]
        except:
            url = "/"
        context.update({'venue_id':self.venue_id,'selected_venue_display_pic':self.selected_venue_display_pic,'venue': self.selected_venue, 'pictures': self.selected_venue_pictures, 'username':username,"venue_ratings":self.venue_ratings,"back_to_search":url})
        return context

class UpdatePackageView(View):
    def post(self,request,*args,**kwargs):
        data = request.POST.get("package-description")
        package_list = json.loads(data)
        quote_id = int(request.POST.get("quote_id"))
        quote = Quote.objects.get(id = quote_id)
        meal_type = 2
        meal_type_obj = MealType.objects.get(id=2)
        name=str(quote_id)+" "+datetime.datetime.now().strftime("%I:%M%p on %B %d, %Y")
        package = Package.objects.create(name=name,meal_type=meal_type_obj,custom=1)
        for option in package_list:
            contents = MealCategoryItemOption.objects.get(id=int(option))
            PackageContents.objects.create(package=package,contents=contents)
        quote.package = package
        quote.save()
        request.session['f_and_b']=True
        return redirect("/shortlist/?quote_id="+str(quote_id))

    def get(self,request,*args,**kwargs):
        package_id = int(request.GET.get("package_id"))
        quote_id = int(request.GET.get("quote_id"))
        package = Package.objects.get(id=package_id)
        quote = Quote.objects.get(id=quote_id)
        quote.package = package
        quote.save()
        request.session['f_and_b']=True
        return redirect("/shortlist/?quote_id="+str(quote_id))




class CancelPackageView(View):
    def post(self,request,*args,**kwargs):
        quote_id = int(request.POST.get("quote_id"))
        quote = Quote.objects.get(id = quote_id)
        quote.package=None
        quote.save()
        request.session['f_and_b']=False
        return redirect("/shortlist/?quote_id="+str(quote.id))

class SubmitReviewView(View):
    def post(self,request,*args,**kwargs):
        venue_id = int(request.POST.get("venue_id"))
        try:
            review = str(request.POST.get("review"))
        except:
            review = ""
        try:
            score = float(request.POST.get("score"))
        except:
            score = float(0)
        user_id = int(request.user.id)
        user = User.objects.get(id=user_id)
        venue = Venue.objects.get(id=venue_id)
        VenueRating.objects.create(venue=venue,user=user,review=review,rating=score)
        return redirect("/venue/?venue="+str(venue_id))


class ContactView(TemplateView):
    template_name = 'venueregistration/contact.html'


    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, self.get_context_data())

    def get_context_data(self, **kwargs):
        context = super(ContactView, self).get_context_data(**kwargs)
        context.update({'message':"hello"})
        return context



class ContactSubmitView(View):
    def post(self,request,*args,**kwargs):
        name = request.POST.get("name")
        email = request.POST.get("email")
        try:
            mobile = int(request.POST.get("mobile"))
        except:
            mobile = 0
        company = request.POST.get("company")
        Contact.objects.create(name=name,mobile=mobile,email=email,company=company)
        return redirect("/contact")



class AboutView(TemplateView):
    template_name = 'venueregistration/about.html'


    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, self.get_context_data())

    def get_context_data(self, **kwargs):
        context = super(AboutView, self).get_context_data(**kwargs)
        context.update({'message':"hello"})
        return context



class PopularVenuesView(View):
    def get(self,request,*args,**kwargs):
        city_id = int(request.GET.get('city_id'))
        event_type = int(request.GET.get('occasion'))
        city = City.objects.get(pk=city_id)
        locations = Location.objects.filter(city=city)
        venues = Venue.objects.filter(location__in=locations,event_type=event_type)
        venuePics = VenuePicture.objects.filter(venue__in=venues,is_display_pic=1)
        venue_dict = {}

        for venuePic in venuePics:
            venue = venuePic.venue
            venue_dict[venuePic.venue.id]={"name":venue.name,"address1":venue.address1,"address2":venue.address2,"address3":venue.address3,"imgUrl":"/media/"+str(venuePic.image)}


        response = HttpResponse(content_type="application/json")
        response.write(json.dumps(venue_dict))
        return response



class SuggestVenuesView(View):
    def post(self,request,*args,**kwargs):
        name = request.POST.get("name")
        venue = request.POST.get("venue")
        location = request.POST.get("location")
        email = request.POST.get("email")
        SuggestVenue.objects.create(name=name,venue=venue,location=location,email=email)

        resp_str = {"status":"Success!"}
        response = HttpResponse(content_type="application/json")
        response.write(json.dumps(resp_str))
        return response



class SetFavourteVenueView(View):
    def get(self,request,*args,**kwargs):
        try:
            user_id = int(request.user.id)
            venue_id = int(request.GET.get("venue_id"))
            if request.GET.get("toggle") is not None and request.GET.get("toggle") != "":
                toggle=request.GET.get("toggle")
            else:
                toggle="add"

            user = User.objects.get(pk=user_id)
            venue = Venue.objects.get(pk=venue_id)
            if toggle == "add":
                fav = Favourite.objects.create(user=user,venue=venue)
            else:
                instance = Favourite.objects.filter(user=user,venue=venue).delete()
            status = "Success"
        except:
            status="Failure"

        response_str = {"status":status}
        response = HttpResponse(content_type="application_json")
        response.write(json.dumps(response_str))
        return response


class GetFavouriteVenueView(View):
    def get(self,request,*args,**kwargs):
        try:
            user_id = int(request.user.id)
            user = User.objects.get(pk=user_id)
            instances = Favourite.objects.filter(user=user)
            fav_list = []
            for instance in instances:
                venue_id = instance.venue.id
                fav_list.append(venue_id)
        except:
            fav_list=[]


        response = HttpResponse(content_type="application_json")
        response.write(json.dumps(fav_list))
        return response



class ForgotPasswordView(View):
    def post(self,request,*args,**kwargs):
        try:
            username = request.POST.get("username")
            user = User.objects.get(username__exact=username)
            temp_password = id_generator()
            to=[user.email]
            user.set_password(str(temp_password))
            user.save();
            send_mail_registration(to,temp_password)
            status="Success"
            message="Your new password has been emailed to you"
        except:
            status = "Failure"
            message = "You have entered a wrong email address/username"

        response_str = {"status":status,"message":message}
        response = HttpResponse(content_type="application_json")
        response.write(json.dumps(response_str))
        return response


class MyAccountView(TemplateView):
    template_name = 'venueregistration/myaccount.html'


    def get(self, request, *args, **kwargs):
        self.tabindex=5
        user = User.objects.get(pk=request.user.id)
        user_profile = user.get_profile()
        self.is_verified = user_profile.is_verified
        if request.GET.get("tab") is not None and request.GET.get("tab")!="":
            self.tabindex = request.GET.get("tab")
        
        self.favourites = Favourite.objects.filter(user=user)
        self.favourites_with_pictures={}
        for favourite in self.favourites:
            self.favourites_with_pictures[favourite.venue.id]=VenuePicture.objects.get(venue=favourite.venue,is_display_pic=True)
        self.quotes = Quote.objects.filter(user=user)
        self.recently_viewed = RecentlyViewed.objects.filter(user=user).values_list('venue', flat=True).distinct()
        self.recently_viewed_with_pictures = {}
        for recent in self.recently_viewed:
            venue = Venue.objects.get(pk=recent)
            self.recently_viewed_with_pictures[venue.id]=VenuePicture.objects.get(venue=venue,is_display_pic=True)
        self.shortlisted_venues = QuoteVenue.objects.filter(quote__in=self.quotes).values_list('venue', flat=True).distinct()
        self.shortlisted_venues_with_pictures = {}
        for shortlisted_venue in self.shortlisted_venues:
            venue = Venue.objects.get(pk=shortlisted_venue)
            self.shortlisted_venues_with_pictures[venue.id]=VenuePicture.objects.get(venue=venue,is_display_pic=True)
        self.quote_venues = {}
        for quote in self.quotes:
            self.quote_venues[quote.id]=QuoteVenue.objects.filter(quote=quote)
        
        return render(request, self.template_name, self.get_context_data())

    def get_context_data(self, **kwargs):
        context = super(MyAccountView, self).get_context_data(**kwargs)
        context.update({'tabindex':self.tabindex,'is_verified':self.is_verified,'favourites':self.favourites_with_pictures,'quotes':self.quote_venues,'shortlist':self.shortlisted_venues_with_pictures,'recently_viewed':self.recently_viewed_with_pictures})
        return context


class UpdateContactView(TemplateView):
    def post(self,request,*args,**kwargs):
        name = request.POST.get("name")
        email = request.POST.get("email")
        company = request.POST.get("company")
        mobile = int(request.POST.get("mobile"))
        user = User.objects.get(pk=request.user.id)
        user_profile = user.get_profile()
        user_profile.company=company
        user_profile.mobile_number = mobile
        user.first_name=name
        user.email=email
        user_profile.save()
        user.save()
        return redirect("/myaccount")
    
    
    
class UpdatePasswordView(View):
    def post(self,request,*args,**kwargs):
        try:
            user = User.objects.get(pk=request.user.id)
            password = request.POST.get("password")
            confirm_password = request.POST.get("confirm_password")
            if password == confirm_password :
                user.set_password(str(password))
                user.save();
                status = "Success"
                message = "Your password has been changed"
            else:
                status = "Failure"
                message = "Your passwords do not match"
        except:
            status = "Failure"
            message = "Unable to change your password please try again later."
        
        response_str = {"status":status,"message":message}
        response = HttpResponse(content_type="application_json")
        response.write(json.dumps(response_str))
        return response
    
class ResendOTPView(View):
    def post(self,request,*args,**kwargs):
        try:
            user = User.objects.get(pk=request.user.id)
            otp = OTP.objects.get(user=user)            
            pin = id_generator()
            otp.password=pin
            otp.save()
            to=[user.email]
            user_profile = user.get_profile()
            
            to_num=user_profile.mobile_number
            to.append(to_num)
            send_sms_msg(to, pin)
            status = "Success"
            message = "Your OTP has been reset"
        except:
            status="Failure"
            message = "Unable to resend OTP please try again later"

        response_str = {"status":status,"message":message}
        response = HttpResponse(content_type="application_json")
        response.write(json.dumps(response_str))
        return response
    
    
class ImageRenderer(View):
    def get(self,request,*args,**kwargs):
        venue_id = int(request.GET.get("venue_id"))
        venue = Venue.objects.get(pk=venue_id)
        venue_picture = VenuePicture.objects.get(venue=venue,is_display_pic = True)
        image_loc = venue_picture.image
        return HttpResponse(image_loc,mimetype="image")
    

class ShowQuoteView(FormView):
    template_name = 'venueregistration/showquotes.html'


    def get(self, request, *args, **kwargs):
        self.selected_venues = Venue.objects.all()
        self.selected_venue_pictures = VenuePicture.objects.filter(is_display_pic=True)
        self.selected_venues_with_pictures = {}
        quote_id = int(request.GET.get("quote_id"))
        quote = Quote.objects.get(pk=quote_id)
        try:
            occasion = quote.occasion
        except:
            occasion = None
        
        dateNeeded = quote.date_of_booking
        
        environment = quote.venuetype
        try:
            guestsModel = quote.guests
        except:
            guestModel = None
        try:
            occasionModel = EventType.objects.filter(id=occasion)[:1].get()
        except:
            occasionModel = None

        try:
            venueTypeModel = VenueType.objects.filter(id=environment)[:1].get()
        except:
            venueTypeModel = None


            
            self.quote = quote
            venue_quotes = QuoteVenue.objects.filter(quote_id=quote_id)
            venue_ids = []
            for venue_quote in venue_quotes:
                venue_ids.append(venue_quote.venue.id)
        
        self.guestsModel = guestsModel
        self.venue_ids = venue_ids
        self.selected_venues = self.selected_venues.filter(id__in=venue_ids)
        self.selected_venue_pictures = self.selected_venue_pictures.filter(venue__in=self.selected_venues)
        venue_names = []
        for venue in self.selected_venues:
            venue_names.append(venue.name)
            self.selected_venues_with_pictures[venue.id] = self.selected_venue_pictures.get(venue=venue)
            
        self.selected_venues_dict = {}
        for venue in self.selected_venues:
            self.selected_venues_dict[venue.id] = venue
        self.f_and_b = False
        if (quote.package is None) :
            self.f_and_b = True

        self.related_venues = RelatedVenues.objects.filter(venue__in=self.selected_venues)
        self.related_venues_with_pictures={}
        self.venue_string = ""
        for venue_name in venue_names:
            self.venue_string=self.venue_string+str(venue_name)+","
        for venue in self.related_venues:
            self.related_venues_with_pictures[venue.related_venue.id]=VenuePicture.objects.get(venue=venue.related_venue,is_display_pic=True)
        return render(request, self.template_name, self.get_context_data())

    def get_context_data(self, **kwargs):
        context = super(ShowQuoteView, self).get_context_data(**kwargs)
        username = ""
        if (self.request.user is None):
            username = ""
        elif (self.request.user.is_authenticated()):
            username = self.request.user.username
        else:
            username = ""

        self.form_class = GetQuoteForm(initial={'name':"", 'address':"", 'company':"", 'email':"", 'mobile':"", 'f_and_b':self.f_and_b})
        context.update({'venue_string':self.venue_string.rstrip(","),'related_venues_pictures':self.related_venues_with_pictures,'guests':self.guestsModel,'selected_venues_with_pictures': self.selected_venues_with_pictures, "selected_venues":self.selected_venues_dict, "username": username, "venue_ids":self.venue_ids, "form":self.form_class,"back_to_search":self.request.session["search_request"],"quote":self.quote})
        return context

    
